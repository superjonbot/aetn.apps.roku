Function get_registry_section(section, key As String) As Dynamic
    
    sec = CreateObject("roRegistrySection", section)

    if sec.Exists(key)
     return sec.Read(key)
    end if

    return Invalid

End Function

 
Function set_registry_section(section, key As String, value As Dynamic) As Boolean

    sec = CreateObject("roRegistrySection", section)

    data = ""

    if isstr(value)
        data = value
    else
        'data = rdSerialize(value, "JSON")
        data = toJSON(value, 5)
    end if

    if sec.Write(key, data)
        return true
    else
        return false
    end if

End Function


Function set_registry_flush(section) As Void

    sec = CreateObject("roRegistrySection", section)

    sec.Flush()

End Function


Function delete_registry_section(section, key="all" As String) As Void

    sec = CreateObject("roRegistrySection", section)

    if key = "all"
        for each eachkey in sec.GetKeyList()
            sec.Delete(eachkey)
        next
    else
        if sec.Exists(key)
            sec.Delete(key)
        end if
    end if

    sec.Flush()

End Function

Function clearAllDataInSection(section) 
    sec = CreateObject("roRegistrySection", section)
    keyList = sec.GetKeyList()
    for each key in keyList
        sec.Delete(key)
    end for
End Function