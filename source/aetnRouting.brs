Function handleDeepLinking(args as Dynamic)

    urlParts  = strSplit(args["url"], "/")
    aetn_log("info", "aetnUtils : handleDeepLinking", "Args:", args)

    content = {}
    if urlParts[1] = "home"
        content["AETNContentType"] = "home"
        aetn_show_screen(content)
    else if urlParts[1] = "select"
        content["AETNContentID"] = urlParts[2]
        content["AETNContentType"] = "video"
        aetn_show_screen(content)
    else if urlParts[1] = "show"
        content["AETNContentID"] = urlParts[2]
        content["AETNContentType"] = "show"
        aetn_show_screen(content)
    else if urlParts[1] = "movie"
        content["AETNContentID"] = urlParts[2]
        content["AETNContentType"] = "movie"
        aetn_show_screen(content)
    else if urlParts[1] = "topic"
        content["AETNContentID"] = urlParts[2]
        content["AETNContentType"] = "topic"
        aetn_show_screen(content)
    end if

End Function

Function aetn_show_screen(content)

    aetn_log("info", "aetnUtils : aetn_show_screen", "AETN Show Screen: ", content.AETNContentType)
    if content.AETNContentType = "search"
        screen = preSearchScreen()
        showSearchScreen(screen)
    else if content.AETNContentType = "searchResult"
        screen = preSearchResultsScreen()
        showSearchResultsScreen(screen, content)
    else if content.AETNContentType = "signin"
        screen = preSignInScreen()
        showSignInScreen(screen)
    else if content.AETNContentType = "signout"
        signout()
    else if content.AETNContentType = "show"
        screen = preShowsScreen()
        showShowsScreen(screen, content)
    else if content.AETNContentType = "topic"
        screen = preTopicsScreen()
        showTopicsScreen(screen, content)
    else if content.AETNContentType = "video"
        screen = preVideoScreen()
        showVideoScreen(screen, content)
    else if content.AETNContentType = "movie"
        screen = preVideoScreen()
        showVideoScreen(screen, content)
    else if content.AETNContentType = "player"
        screen = prePlayerScreen()
        showPlayerScreen(screen, content)
    else if content.AETNContentType = "watchlist"
        screen = preWatchlistScreen()
        showWatchlistScreen(screen, content)
    else if content.AETNContentType = "info"
        showInfoDialog()
    else if content.AETNContentType = "home"
        screen = preHomeScreen()
        showHomeScreen(screen)
    end if

End Function