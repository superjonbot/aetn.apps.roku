Function preSearchScreen()

    port = CreateObject("roMessagePort")

    screen = CreateObject("roSearchScreen")
    screen.SetMessagePort(port)
    screen.SetBreadcrumbText("", "Search")
    screen.SetSearchTermHeaderText("Suggestions:")
    screen.SetSearchButtonText("Search")
    screen.SetClearButtonEnabled(false)

    return screen

End Function


Function showSearchScreen(screen As Object) As Integer

    if validateParam(screen, "roSearchScreen", "showSearchScreen") = false return -1

    history = CreateObject("roArray", 1, true)

    displayHistory = true

    searchHistory = CreateObject("roSearchHistory")
    list = searchHistory.GetAsArray()

    if displayHistory
        screen.SetSearchTermHeaderText("Search by title: ")
        screen.SetSearchButtonText("Search")
        screen.SetClearButtonText("Clear history")
        screen.SetClearButtonEnabled(true) 'defaults to true
        screen.SetSearchTerms(list)
    else
        screen.SetSearchTermHeaderText("Search Suggestions:")
        screen.SetSearchButtonText("Search")
        screen.SetClearButtonEnabled(false)
    end if 

    screen.Show()
    aetnOmniture("Search")
    shows = GetAllShows()    

    done = false

    while done = false
        msg = wait(0, screen.GetMessagePort()) 
    
        if type(msg) = "roSearchScreenEvent"
    
            if msg.isScreenClosed()
    
                aetn_log("info", "appSearch", "Screen Closed")
                done = true
    
            else if msg.isCleared()
    
                aetn_log("info", "appSearch", "Search Term cleared")
                searchHistory.Clear()
    
            else if msg.isPartialResult()
    
                aetn_log("info", "appSearch", "partial search: " + msg.GetMessage())
    
                if displayHistory    
                    displayHistory = false
                    screen.SetSearchTermHeaderText("Search Suggestions:")
                    screen.SetClearButtonEnabled(false)
                end if

                suggestions = GenerateSearchSuggestions(msg.GetMessage(), shows)
                screen.SetSearchTerms(suggestions)

            else if msg.isFullResult()

                aetn_log("info", "appSearch", "Full Search:" + msg.GetMessage())
                searchTerm = msg.GetMessage()
                searchHistory.Push(searchTerm)  
                contentObject = CreateObject("roAssociativeArray") 
                contentObject.AETNContentType = "searchResult"
                contentObject.searchTerm = searchTerm
                contentObject.shows = shows
                aetn_show_screen(contentObject)

            end if
        end if
    endwhile 

    return 0

End Function


Function GetAllShows() as Object

    feedJSON = aetn_remote_get(m.aetn_context.showFeedURL)
    shows = ParseJSON(feedJSON)
    h2FeedJSON = aetn_remote_get(m.aetn_context.h2ShowFeedURL)
    h2Shows = ParseJSON(h2FeedJSON)

    if shows <> Invalid
        if h2Shows <> Invalid
            shows.append(h2Shows)
        end if
        return shows
    else
        if h2Shows <> Invalid
            return h2Shows
        else
            return {}
        end if
    end if

End Function


Function GenerateSearchSuggestions(partSearchText As String, shows As Object) As Object

    suggestions = CreateObject("roArray", 1, true) 

    length = len(partSearchText)

    for each show in shows 

        if  LCase(show.Title.left(length)) = partSearchText
            suggestions.push(show.Title)
        end if

    end for

    return suggestions
End Function