Function preShowsScreen()

    port = CreateObject("roMessagePort")

    screen = CreateObject("roPosterScreen")
    screen.SetMessagePort(port)
    screen.SetBreadcrumbText("Home", "Show")
    screen.SetDisplayMode("scale-to-fill")
    screen.SetListStyle("flat-episodic-16x9")

    return screen

End Function


Function showShowsScreen(screen As Object, contentObj As Object) As Integer

    if validateParam(screen, "roPosterScreen", "showShowsScreen") = false return -1

    aetn_log("info", "appShow", "Show showScreen")

    m.refreshShowScreen = false

    contentObj = fetchContent(contentObj.AETNContentID, "show", true, contentObj.network)
    if contentObj = Invalid OR contentObj.linkRef = ""
        return -1
    end if

    aetn_log("trace", "appShow", "show content", contentObj)   

    currentIndex = 0
    screen.SetBreadcrumbText("Show", contentObj.title)
    
    screen.Show()
    screenDetail = {}
    screenDetail.pageName = strRemoveSpace(contentObj.Title)
    screenDetail.network = contentObj.network
    aetnOmniture("Shows", screenDetail)
    aetnKrux("screen", "Shows", "", screenDetail)   
    tabs = []
    contentLists = setShowsScreenList(screen, contentObj, tabs)

    while true
        
        msg = wait(0, screen.GetMessagePort())
        if type(msg) = "roPosterScreenEvent"

            aetn_log("info", "appShow", "showShowsScreen | msg = " + msg.GetMessage() + " | index = " + itostr(msg.GetIndex()) + " | column = " + itoStr(msg.getData()))
            if msg.isListFocused()            

                index = msg.GetIndex()
                currentIndex = index
                screen.SetContentList(contentLists[index])
                screen.SetFocusedListItem(0)
                screenDetail.pageName = strRemoveSpace(contentObj.Title + ":" + tabs[index])
                aetnOmniture("Shows", screenDetail)

            else if msg.isListItemSelected()
            
                index = msg.GetIndex()
                aetn_show_screen(contentLists[currentIndex][index])
                if m.refreshShowScreen = true
                    setShowsScreenList(screen, contentObj, tabs)
                    m.refreshShowScreen = false
                end if
            
            else if msg.isScreenClosed()
            
                return -1
            
            end if
        
        end if
    
    end while

    return 0

End Function


Function setShowsScreenNav(screen As Object)

    if validateParam(screen, "roPosterScreen", "setNavigationTitles") = false return -1
    timer3 = CreateObject("roTimespan")
    timer3.Mark()
    showsJSON = aetn_remote_get(m.aetn_context.showFeedURL, false, [], true)
    aetn_log("trace", "appShow", "get show json in " + itostr(timer3.TotalMilliseconds()) + "ms")
    showsArray = ParseJSON(showsJSON)

    if showsArray = Invalid
        showsArray = {}
    end if
    
    screen.SetupLists(showsArray.Count())
    
    nav = []
    for each item in showsArray
        nav.push(item.showID)
    next

    screen.SetListNames(nav)

End Function

Function sortByEpisode(show)

    if show.episode <> Invalid
        return show.episode.toint()
    end if
        
    return -1
End Function

Function sortBySeason(show)

    if show.season <> Invalid
        return show.season.toint()
    end if
        
    return -1
End Function


Function setShowsScreenList(screen As Object, contentObj As Object, tabs)

    'tabs = []
    allVideosArray = []
    contentLists = []
    
    tabIndex = 0
    timer4 = CreateObject("roTimespan")
    timer4.Mark()
    episodeJSON = aetn_remote_get(contentObj.episodeFeedURL, false, [], true)
    aetn_log("trace", "appShows", "get episode feed in " + itostr(timer4.TotalMilliseconds()) + "ms")
    episodeVideosJSON = ParseJSON(episodeJSON)
    
    if episodeVideosJSON <> Invalid

        episodeArray = episodeVideosJSON
        for each episode in episodeArray
            if episode.season = Invalid
                episode.season = "0"
            end if

            if episode.episode = Invalid
                episode.episode = "0"
            end if
        next
        
        if episodeArray.Count() > 0 
            tabs.push("All Episodes")
            contentLists[tabIndex] = episodeArray
            tabIndex = tabIndex + 1
        end if
        
        'Build a seasons  array
        seasons = CreateObject("roAssociativeArray")
        unlocked = []
        unlockedTabIndex = -1

        for each episode in episodeArray

            if episode.isBehindWall <> "true"
                unlocked.push(episode)
            end if
        
            if seasons[episode.season] = Invalid 
                seasons[episode.season] = []
            end if

            if m.aetn_token <> Invalid
                episode["ShortDescriptionLine2"] = ""
            end if

            seasons[episode.season].push(episode)
        next

        if m.aetn_token = Invalid

            if unlocked <> Invalid and unlocked.Count() <> 0
                tabs.push("Unlocked Episodes") 
                contentLists[tabIndex] = unlocked
                unlockedTabIndex = tabIndex
                tabIndex =  tabIndex + 1
            end if

        end if
        
        keys = []
        for each key in seasons
            keys.push(key.toInt())
        next
        sort(keys)

        for each key in keys
            strKey = toStr(key)
            if seasons[strKey].Count() > 0 and strKey <> "0"
                tabs.push("Season " +  strKey) 
                contentLists[tabIndex] = seasons[strKey]
                tabIndex =  tabIndex + 1
            end if
        next

    end if

    timer5 = CreateObject("roTimespan")
    timer5.Mark()
    clipsJSON = aetn_remote_get(contentObj.clipFeedURL, false, [], true)
    aetn_log("trace", "appShows", "Get Clip Feed in " + itostr(timer5.TotalMilliseconds()) + "ms")
    clipsVideosJSON = ParseJSON(clipsJSON)

    clipsArray = Invalid

    if clipsVideosJSON <> Invalid

        clipsArray = clipsVideosJSON
        if clipsArray.Count() > 0 
            tabs.push("Clips")
            contentLists[tabIndex] = clipsArray
        end if

    end if

    if clipsArray <> Invalid and episodeArray <> Invalid
        if ( clipsArray.Count() + episodeArray.Count() ) < 1
            aetn_error("No videos found")
            return -1
        end if
        screen.SetListNames(tabs)
        screen.SetContentList(contentLists[0])    
        
    end if

    if m.aetn_token = Invalid and unlockedTabIndex <> -1
        screen.SetFocusedList(unlockedTabIndex)
    else 
        screen.SetFocusedList(0)
    end if

    return contentLists

End Function