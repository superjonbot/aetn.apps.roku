Function preHomeScreen()

    port = CreateObject("roMessagePort")

    screen = CreateObject("roGridScreen")
    screen.SetMessagePort(port)
    screen.SetBreadcrumbText("Home","")
    screen.SetDisplayMode("scale-to-fill")
    screen.SetGridStyle("two-row-flat-landscape-custom")

    return screen

End Function


Function showHomeScreen(screen As Object) As Integer

    if validateParam(screen, "roGridScreen", "showHomeScreen") = false return -1

    aetn_log("info", "appHome", "Show Home Screen")

    m.refreshHomeScreen = false

    setHomeScreenNav(screen)
    aetnOmniture("Home")
    aetnKrux("screen", "Featured", "")
    grid = setHomeScreenGrid(screen)
    screen.Show()

    if m.fireInitialAuthCall <> true
        aetnOmniture("Authentication")
        m.fireInitialAuthCall = true
    end if

    while true
        
        msg = wait(0, screen.GetMessagePort())

        if type(msg) = "roGridScreenEvent"
            aetn_log("info", "appHome",  "showHomeScreen | msg = " + msg.GetMessage() + " | index = " + itostr(msg.GetIndex()) + " | column = "+ itostr(msg.GetData()))
            if msg.isListItemSelected()
            
                row = msg.GetIndex()
                column = msg.GetData()
                aetn_show_screen(grid[row][column])
                if m.refreshHomeScreen = true    
                    setHomeScreenNav(screen)
                    grid = setHomeScreenGrid(screen)
                    m.refreshHomeScreen = false
                end if
            else if msg.isRemoteKeyPressed()
                menu = {}
                menu.AETNContentType = "menu"
                menuitem = showMenuDialog()
                
                content = {}
                content.AETNContentType = menuitem
                aetn_show_screen(content)
                if m.refreshHomeScreen = true    
                    setHomeScreenNav(screen)
                    grid = setHomeScreenGrid(screen)
                    m.refreshHomeScreen = false
                end if
                
            else if msg.isScreenClosed()
                return -1
            end if

        end if
    end while

    return 0

End Function

Function updateWatchlistRow(screen As Object, grid, index)
    error_in_data = false

    for each item in m.aetn_context.rokuHome
        if item.type = "localstorage"

            localStorageData = get_registry_section(m.aetn_context.rokulocalstorage.section, m.aetn_context.rokulocalstorage[item.key])

            if localStorageData = Invalid
                error_in_data = true
            else
                parsedLocalStorageData = ParseJSON(localStorageData)
                aetn_log("debug","appHome","Parse Local Storage Data", parsedLocalStorageData, "Local Storage Data")
                if parsedLocalStorageData = Invalid
                    error_in_data = true
                    contentList = []
                else
                    contentList = parsedLocalStorageData
                end if
            end if

            if error_in_data
                aetn_error("Error parsing watchlist")
            else    
                screen.SetContentList(index, contentList)
                grid[index] = contentList
            end if

            if contentList.count() = 0
                screen.SetListVisible(index, false)
            else
                screen.SetListVisible(index, true)
            end if

       end if
    next

End Function

Function updateUtilitiesRow(screen As Object, grid, index)

    for each item in m.aetn_context.rokuHome
        if item.type = "json"
            utilities = item.data
            contentList = setUtilityIcons(utilities)
            screen.SetContentList(index, contentList)
            grid[index] = contentList
        end if
    next

End Function

Function setHomeScreenNav(screen As Object)

    if validateParam(screen, "roGridScreen", "setNavigationTitles") = false return -1

    screen.SetupLists(m.aetn_context.rokuHome.Count())
    
    nav = []
    for each item in m.aetn_context.rokuHome
        nav.push(item.title)
    next

    screen.SetListNames(nav)

End Function

Function setUtilityIcons(utilities As Object)

    filteredUtils = []

    for each utility in utilities        
        if utility.AETNContentType = "signin" 
            if m.aetn_token = Invalid
                filteredUtils.push(utility)
            end if
        else if utility.AETNContentType = "signout"
            if m.aetn_token <> Invalid
                filteredUtils.push(utility)
            end if
        else
            filteredUtils.push(utility)
        end if
    next

    return filteredUtils

End Function

    
Function setHomeScreenGrid(screen As Object)

    index = 0
    grid  = []
    error_in_data = false

    aetn_log("info", "appHome", "set Home Screen Grid")

    m.aetn_context["featureFeedURL"] = m.aetn_context.featureFeedURL

    for each item in m.aetn_context.rokuHome
        error_in_data = false

        if item.type = "feed"

            timer1 = CreateObject("roTimespan")
            timer1.Mark()
            feedJSON = aetn_remote_get( m.aetn_context[item.feedVar], false, [], true )
            aetn_log("trace", "appHome", "this feed took: " + itostr(timer1.TotalMilliseconds()) + "ms", m.aetn_context[item.feedVar], "Feed url")

            feedData = ParseJSON(feedJSON)

            if feedData <> Invalid
                contentList = feedData
            else
                error_in_data = true
            end if           

        else if item.type = "json"
            m.menuData = item.data
            contentList = setUtilityIcons(item.data)

        else if item.type = "localstorage"
            localStorageData = get_registry_section(m.aetn_context.rokulocalstorage.section, m.aetn_context.rokulocalstorage[item.key])
            if localStorageData = Invalid
                error_in_data = true
            else
                parsedLocalStorageData = ParseJSON(localStorageData)
                if parsedLocalStorageData = Invalid
                    error_in_data = true
                else
                    contentList = parsedLocalStorageData

                    if m.juststarted = true
                        newContentList = []
                        ids = ""
                        For Each content In contentList
                            if content.aetncontenttype = "video"
                                ids = ids + content.aetncontentid + ","
                            else
                                newContentList.push(content)
                            end if
                        End For
                        ids = Left(ids, Len(ids) - 1)

                        validVideos = fetchVideos(ids)

                        For Each video in contentList
                            if (not validVideos.DoesExist(video.aetncontentid))                            
                                if item.key = "watchlistkey"
                                    print "remove from watchlist"
                                    deleteFromWatchList(video)
                                else if item.key = "continuekey"
                                    print "remove from continue list"
                                    removeFromContinueWatching(video)
                                end if 
                            else 
                                newContentList.push(video)
                            end if
                        End For

                        contentList = newContentList
                                                
                    end if

                    m.juststarted = false
                end if
            end if
            
        end if

        if contentList <> Invalid
            screen.SetContentList(index, contentList)
            grid[index] = contentList
        
            if error_in_data OR contentList.Count() = 0
                screen.SetListVisible(index, false)
            end if

            index = index + 1
        end if
        
    next

    return grid

End Function

Function showMenuDialog()

    port = CreateObject("roMessagePort")
    dialog = CreateObject("roMessageDialog")
    dialog.SetMessagePort(port) 
    'dialog.SetTitle("Menu")
    menuList = []
    
    for each item in m.menuData
        if item.AETNContentType = "signin" 
            if m.aetn_token = Invalid
                'dialog.AddButton(count, menu.title)
                menuList.push(item)
            end if
        else if item.AETNContentType = "signout"
            if m.aetn_token <> Invalid
                'dialog.AddButton(count, menu.title)
                menuList.push(item)
            end if
        else
            'dialog.AddButton(count, menu.title)
            menuList.push(item)
        end if 
    end for

    counter = 0
    for each menu in menuList
        dialog.AddButton(counter, menu.title)
        counter = counter + 1
    end for
 
    
    dialog.AddButton(counter, "close")

    dialog.EnableBackButton(true)
    dialog.Show()
    
    while True
        dlgMsg = wait(0, dialog.GetMessagePort())
        if type(dlgMsg) = "roMessageDialogEvent"
            if dlgMsg.isButtonPressed()
                if dlgMsg.GetIndex() < menuList.count()
                    return menuList[dlgMsg.GetIndex()].AETNContentType                
                else
                    exit while
                end if
            else if dlgMsg.isScreenClosed()
                exit while
            end if
        end if
    end while


End Function