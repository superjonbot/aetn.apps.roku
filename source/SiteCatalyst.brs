' *********************************************************
' **  SiteCatalyst Implementation Library
' **  Data Insertion API (HTTP GET method ONLY)
' **  v0.7 August 2013 by Eric Hansen
' **  Copyright (c) 2011-2013 Adobe Systems Inc. All Rights Reserved.
' **
' **  Changelog:
' **  0.7 - Added support for Context Data Variables (nested arrays apparently not supported)
' **  0.6 - Added support for Light Server Calls
' **  0.5 - Initial alpha release supporting Data Insertion API calls
' **  
' **  Features to address:
' **  Asynchronous Image Requests
' **  Compression (gzip) header support
' **  XML support
' **  Media Module support
' *********************************************************

' *********************************************************
' ** Configuration Section
' ** Use the OmnitureMeasurement function to create a 
' ** SiteCatalyst tracking object with standard variables
' *********************************************************
Function OmnitureMeasurement(rsid="" As String) As Object
	
	' *********************************************************
	' ** Define s tracking object variables
	' ** Default values set (cookies not supported on the Roku)
	' *********************************************************
	s = {
		account:rsid 'required
		linkURL:""
		linkName:""
		linkType:""
		dc:""
		trackingServer:""
		trackingServerSecure:""
		userAgent:"" 
		dynamicVariablePrefix:"" 
		visitorID:getVisitorID() 'required. no longer defaults to Device Hardware ID
		vmk:""
		visitorMigrationKey:""
		visitorMigrationServer:""
		visitorMigrationServerSecure:""
		charSet:"UTF-8"
		visitorNamespace:""
		pageName:"" 'required
		pageURL:"" 'required
		referrer:""
		currencyCode:""
		purchaseID:""
		variableProvider:""
		channel:""
		server:""
		pageType:""
		transactionID:""
		campaign:""
		state:""
		zip:""
		events:""
		products:""
		hier1:""
		hier2:""
		hier3:""
		hier4:""
		hier5:""
		prop1:""
		prop2:""
		prop3:""
		prop4:""
		prop5:""
		prop6:""
		prop7:""
		prop8:""
		prop9:""
		prop10:""
		prop11:""
		prop12:""
		prop13:""
		prop14:""
		prop15:""
		prop16:""
		prop17:""
		prop18:""
		prop19:""
		prop20:""
		prop21:""
		prop22:""
		prop23:""
		prop24:""
		prop25:""
		prop26:""
		prop27:""
		prop28:""
		prop29:""
		prop30:""
		prop31:""
		prop32:""
		prop33:""
		prop34:""
		prop35:""
		prop36:""
		prop37:""
		prop38:""
		prop39:""
		prop40:""
		prop41:""
		prop42:""
		prop43:""
		prop44:""
		prop45:""
		prop46:""
		prop47:""
		prop48:""
		prop49:""
		prop50:""
		prop51:""
		prop52:""
		prop53:""
		prop54:""
		prop55:""
		prop56:""
		prop57:""
		prop58:""
		prop59:""
		prop60:""
		prop61:""
		prop62:""
		prop63:""
		prop64:""
		prop65:""
		prop66:""
		prop67:""
		prop68:""
		prop69:""
		prop70:""
		prop71:""
		prop72:""
		prop73:""
		prop74:""
		prop75:""
		eVar1:""
		eVar2:""
		eVar3:""
		eVar4:""
		eVar5:""
		eVar6:""
		eVar7:""
		eVar8:""
		eVar9:""
		eVar10:""
		eVar11:""
		eVar12:""
		eVar13:""
		eVar14:""
		eVar15:""
		eVar16:""
		eVar17:""
		eVar18:""
		eVar19:""
		eVar20:""
		eVar21:""
		eVar22:""
		eVar23:""
		eVar24:""
		eVar25:""
		eVar26:""
		eVar27:""
		eVar28:""
		eVar29:""
		eVar30:""
		eVar31:""
		eVar32:""
		eVar33:""
		eVar34:""
		eVar35:""
		eVar36:""
		eVar37:""
		eVar38:""
		eVar39:""
		eVar40:""
		eVar41:""
		eVar42:""
		eVar43:""
		eVar44:""
		eVar45:""
		eVar46:""
		eVar47:""
		eVar48:""
		eVar49:""
		eVar50:""
		eVar51:""
		eVar52:""
		eVar53:""
		eVar54:""
		eVar55:""
		eVar56:""
		eVar57:""
		eVar58:""
		eVar59:""
		eVar60:""
		eVar61:""
		eVar62:""
		eVar63:""
		eVar64:""
		eVar65:""
		eVar66:""
		eVar67:""
		eVar68:""
		eVar69:""
		eVar70:""
		eVar71:""
		eVar72:""
		eVar73:""
		eVar74:""
		eVar75:""
		list1:""
		list2:""
		list3:""
		timestamp:""
		profileID:""
		ssl:false
		debugTracking:false
		autoTimestamp:false
	}
	
Return s

End Function

' *********************************************************
' ** PLUG-INS
' *********************************************************

' Visitor ID As String
Function getVisitorID() As String
	'No way to get the Roku Account ID instead, unfortunately
	di = CreateObject("roDeviceInfo")
	Return di.GetDeviceUniqueId()
End Function

' Device As String
Function getDeviceModel() As String
	di = CreateObject("roDeviceInfo")
	Return di.GetModel()
End Function

' Version As String
Function getFirmwareVersion() As String
	di = CreateObject("roDeviceInfo")
	Return di.GetVersion()
End Function

' Features as String (comma-delimited list)
Function getFeaturesList() As String
	di = CreateObject("roDeviceInfo")
	list = ""
	If di.HasFeature("5.1_surround_sound") Then
		If Len(list) > 0 Then 
			list = list + ", "
		End If
		list = list + "5.1 Surround Sound"
	End If
	
	If di.HasFeature("sd_only_hardware") Then
		If Len(list) > 0 Then 
			list = list + ", "
		End If
		list = list + "SD"
	End If
	
	If di.HasFeature("usb_hardware") Then
		If Len(list) > 0 Then 
			list = list + ", "
		End If
		list = list + "USB"
	End If
	
	If di.HasFeature("1080p_hardware") Then
		If Len(list) > 0 Then 
			list = list + ", "
		End If
		list = list + "HD"
	End If
	
	Return list
End Function

' Timezone as String
Function getTimeZoneInfo() As String
	di = CreateObject("roDeviceInfo")
	Return di.GetTimeZone()
End Function

' Display Resolution settings as String
' Parameters:
'   dmode - Boolean, add Display Mode (720p, 480i, etc.) to result string
'   dtype - Boolean, add Display Type (HDTV, 4x3 standard) to result string
'   dratio - Boolean, add Screen Aspect Ratio to result string
'   delimiter = String, characters used to separate parts of the result string
Function getResolutionInfo(dmode=true,dtype=true,dratio=false,delimiter=" : ") As String
	di = CreateObject("roDeviceInfo")
	ri = ""
	If dmode Then
		If Len(ri) > 0 Then ri = ri + delimiter
		ri = ri + di.getDisplayMode()
	End If
	
	If dtype Then
		If Len(ri) > 0 Then ri = ri + delimiter
		ri = ri + di.getDisplayType()
	End If
	
	If dratio Then
		If Len(ri) > 0 Then ri = ri + delimiter
		ri = ri + di.getDisplayAspectRatio()
	End If
	
	Return ri
End Function

' *********************************************************
' ** Supporting Routines
' ** ANY CHANGES TO THE CODE BELOW MAY RESULT IN ERRORS
' **
' *********************************************************
Function sendRequest(s As Object, contextData={} As Object) As Integer
	http = CreateObject("roUrlTransfer")
	http.SetPort(CreateObject("roMessagePort"))
	url = "http"
	
	'check for ssl and set inital prefix value if available
	If s.ssl Then 
		url = url + "s"
		prefix = s.trackingServerSecure
	Else
		prefix = s.trackingServer
	End If
	
	url = url + "://"
	
	'prepare the prefix if tracking server is not specified
	If prefix = "" Then
		'fix up the dc variable, default to SJO
		s.dc = LCase(s.dc)
		If s.dc = "dc2" OR s.dc = "122" Then
			s.dc = "122"
		Else
			s.dc = "112"
		End If
		
		If s.visitorNamespace <> "" Then
			prefix = s.visitorNamespace + "." + s.dc + ".207.net"
		Else
			prefix = s.account + "." + s.dc + ".207.net"
		End If
	End If
	
	url = url + prefix + "/b/ss/" + s.account + "/0/BRS-0.7/?AQB=1&ndh=1&"
	
	'build the query string
	qs = ""
	For Each o in s
		
		If s.debugTracking Then
			value = ""
			If type(s[o]) = "roString" and s[o] <> "" Then
				value = s[o]
			End If
		End If
		
		If o = "visitorid" And s[o] <> "" Then
			qs = qs + "vid=" + http.escape(s[o]) + "&"
		Else If o = "pageurl" And len(s[o]) > 0 Then
			qs = qs + "g=" + http.escape(left(s[o], 255)) + "&"
		Else If o = "referrer" And s[o] <> "" Then
			qs = qs + "r=" + http.escape(left(s[o], 255)) + "&"
		Else If o = "events" And s[o] <> "" Then
			qs = qs + "ev=" + http.escape(s[o]) + "&"
		Else If o = "visitormigrationkey" And s[o] <> "" Then
			qs = qs + "vmt=" + s[o] + "&"
		Else If o = "vmk" And s[o] <> "" Then
			qs = qs + "vmt=" + s[o] + "&"
		Else If o = "visitormigrationserver" And s[o] <> "" And s.ssl = false Then
			qs = qs + "vmf=" + s[o] + "&"
		Else If o = "visitormigrationserversecure" And s[o] <> "" And s.ssl = true Then
			qs = qs + "vmf=" + s[o] + "&"
		Else If o = "timestamp" Then
			If s.autoTimestamp Then
				dt = CreateObject("roDateTime")
				dt.mark()
				'autoTimestamp needs escape?
				qs = qs + "ts=" + http.escape(dt.asSeconds().toStr()) + "&"
			Else If s[o] <> "" Then
				qs = qs + "ts=" + http.escape(s[o]) + "&"
			End If
		Else If o = "pagename" And s[o] <> "" Then
			qs = qs + "gn=" + http.escape(s[o]) + "&"
		Else If o = "pagetype" And s[o] <> "" Then
			qs = qs + "gt=" + http.escape(s[o]) + "&"
		Else If o = "products" And s[o] <> "" Then
			qs = qs + "pl=" + http.escape(s[o]) + "&"
		Else If o = "purchaseid" And s[o] <> "" Then
			qs = qs + "pi=" + http.escape(s[o]) + "&"
		Else If o = "server" And s[o] <> "" Then
			qs = qs + "sv=" + http.escape(s[o]) + "&"
		Else If o = "charset" And s[o] <> "" Then
			qs = qs + "ce=" + s[o] + "&"
		Else If o = "visitornamespace" And s[o] <> "" Then
			qs = qs + "ns=" + s[o] + "&"
		Else If o = "currencycode" And s[o] <> "" Then
			qs = qs + "cc=" + s[o] + "&"
		Else If o = "channel" And s[o] <> "" Then
			qs = qs + "ch=" + http.escape(s[o]) + "&"
		Else If o = "transactionid" And s[o] <> "" Then
			qs = qs + "xact=" + http.escape(s[o]) + "&"
		Else If o = "campaign" And s[o] <> "" Then
			qs = qs + "v0=" + http.escape(s[o]) + "&"
		Else If left(o, 4) = "prop" And s[o] <> "" Then
			qs = qs + "c"+mid(o, 5) +"=" + http.escape(s[o]) + "&"
		Else If left(o, 4) = "evar" And s[o] <> "" Then
			qs = qs + "v"+mid(o, 5) +"=" + http.escape(s[o]) + "&"
		Else If left(o, 4) = "list" And s[o] <> "" Then
			qs = qs + "l"+mid(o, 5) +"=" + http.escape(s[o]) + "&"
		Else If left(o, 4) = "hier" And s[o] <> "" Then
			qs = qs + "h"+mid(o, 5) +"=" + http.escape(s[o]) + "&"
		Else If o = "linktype" And s[o] <> "" Then
			qs = qs + "pe=lnk_" + s[o] + "&"
		Else If o = "linkurl" And s[o] <> "" Then
			qs = qs + "pev1=" + http.escape(s[o]) + "&"
		Else If o = "linkname" And s[o] <> "" Then
			qs = qs + "pev2=" + http.escape(s[o]) + "&"
		Else If o = "state" And s[o] <> "" Then
			qs = qs + "state=" + http.escape(s[o]) + "&"
		Else If o = "zip" And s[o] <> "" Then
			qs = qs + "zip=" + http.escape(s[o]) + "&"
		Else If o = "profileID" And s[o] <> "" Then
			qs = qs + "mtp=" + http.escape(s[o]) + "&"
		End If
	End For
	

	contextvars = 0
	
	cqs = "c." + "&"
	For Each key in contextData
		cqs = cqs + http.escape(key) + "=" + http.escape(contextData[key]) + "&"
		contextvars = contextvars + 1
		If s.debugTracking Then
			aetn_log("debug", "siteCatalytic", "context data", contextData[key], key)
		End If
	End For
	cqs = cqs + ".c" + "&"
	
	If contextvars > 0 Then qs = qs + cqs
	
	'cap the query string
	url = url + qs + "AQE=1"
	
	'check for header modifications
	If s.userAgent <> "" Then
		http.AddHeader("User-Agent", s.userAgent)
	End If
	
	'send the request
	http.setUrl(url)
	If s.debugTracking Then 
		aetn_log("debug", "siteCatalytic", "send catalytic request: " + url)
	end if
	http.getToString()
	
	Return 1
	
End Function

' *********************************************************
' ** 		DO NOT CHANGE THE CODE BELOW THIS LINE		 **
' *********************************************************