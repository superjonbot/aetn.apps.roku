Function preSignInScreen()

    port = CreateObject("roMessagePort")

    screen = CreateObject("roCodeRegistrationScreen")
    screen.SetMessagePort(port)
    screen.SetBreadcrumbText("", "")

    screen.AddButton(1, "Get New Code")
    screen.AddButton(2, "Cancel")
    
    screen.AddParagraph("To view this video and even more " + m.aetn_context.adobePass.requestor_name + " videos, you must first link your " + m.aetn_context.adobePass.requestor_name + " app to your TV provider.")

    screen.AddFocalText(" ", "spacing-dense")        
    screen.AddFocalText("From your computer,", "spacing-dense")
    screen.AddFocalText("go to " + m.aetn_context.adobePass.registrationURL, "spacing-dense")
    screen.AddFocalText("and enter this code:", "spacing-dense")
    screen.AddFocalText(" ", "spacing-dense")
    screen.SetRegistrationCode("retreiving code...")

    screen.AddParagraph("This screen will automatically update once your " + m.aetn_context.adobePass.requestor_name + " app has been linked to your TV provider account.")

    return screen

End Function


Function showSignInScreen(screen As Object) As Integer

    if validateParam(screen, "roCodeRegistrationScreen", "showSignInScreen") = false return -1

    aetn_log("info", "appSignIn", "Show Sign In Screen")

    screen.Show()
    aetnOmniture("Authentication", {}, ["TV Everywhere Authentication Start"])

    token = false
    code = getNewRegistrationCode()
    screen.SetRegistrationCode(code)

    retryCount = 0

    while true
        if(retryCount > 40)
            return -1
        end if
        
        msg = wait(15000, screen.GetMessagePort())

        if not isbool(code)
            token = getAuthenticationTokenByCode(code)
        end if

        if not isbool(token)
            m.refreshHomeScreen = true
            m.aetn_token = token
            setMVPDLogo(m.aetn_token.mvpd)
            aetnOmniture("", {}, ["TV Everywhere Authentication Complete"])
            aetn_message("Success", "Your device is now activated.")
            m.refreshShowScreen = true
            return -1

        else if type(msg) = "roCodeRegistrationScreenEvent"
            aetnOmniture("", {}, ["TV Everywhere Authentication Error"])
            
            if msg.isButtonPressed()
                if msg.GetIndex() = 1
                    code = getNewRegistrationCode()
                    screen.SetRegistrationCode(code)
                else if msg.GetIndex() = 2
                    return -1
                end if

            else if msg.isScreenClosed()
                return -1
            end if
        end if

        retryCount = retryCount + 1
    end while

    return 0

End Function

Function signOut()

    aetn_log("info", "appSignIn", "Sign Out")

    url  = m.aetn_context.adobePass.adobePassEndPoint + "/api/v1/logout?deviceId=" + getDeviceESN() + "&resource=" + m.aetn_context.adobePass.requestor_id

    auth = getAuthorizationHeader(m.aetn_context.adobePass.public_key, m.aetn_context.adobePass.private_key, "DELETE", m.aetn_context.adobePass.requestor_id, "/api/v1/logout")

    headers = [
        {
            name : "Authorization",
            value : auth
        }
    ]

    aetn_log("trace", "appSignIn : signOut", "Auth", auth)
    aetn_log("trace", "appSignIn: signOut", "signout url", url)

    aetn_remote_delete(url, false, headers)
    m.refreshHomeScreen = true
    m.aetn_token = Invalid
    removeMVPDLogo()
    aetnOmniture("Authentication")

End Function


Function getAuthenticationToken()

    aetn_log("info", "appSignIn", "Get Authentication Token")

    if m.aetn_context <> invalid and m.aetn_context.adobePass <> invalid

        url  = m.aetn_context.adobePass.adobePassEndPoint + "/api/v1/tokens/authn?format=json&requestor=" + m.aetn_context.adobePass.requestor_id + "&deviceId=" + getDeviceESN()

        auth = getAuthorizationHeader(m.aetn_context.adobePass.public_key, m.aetn_context.adobePass.private_key, "GET", m.aetn_context.adobePass.requestor_id, "/api/v1/tokens/authn")

        headers = [
            {
                name : "Authorization",
                value : auth
            }
        ]

        aetn_log("trace", "appSignIn : getAuthenticationToken", "Auth", auth)
        aetn_log("trace", "appSignIn: getAuthenticationToken", "getAuth url", url)

        token = aetn_remote_get(url, false, headers)

        if token <> ""
            return ParseJSON(token)
        else
            return Invalid
        end if
    else 
        return Invalid
    end if

End Function


Function getAuthenticationTokenByCode(code)

    if m.aetn_context <> invalid and m.aetn_context.adobePass <> invalid

        url = m.aetn_context.adobePass.adobePassEndPoint + "/api/v1/authenticate/" + code + ".json?requestor=" + m.aetn_context.adobePass.requestor_id

        aetn_log("info", "appSignIn", "Get Authentication Token By Code")
        
        token = aetn_remote_get(url)

        if token <> ""
            return ParseJSON(token)
        else
            return false
        end if
    else
        return false
    end if

End Function


Function getAuthorization( video )

    aetn_log("info", "appSignIn", "Get Authorization for video")

    resource = "<rss version='2.0' xmlns:media='http://search.yahoo.com/mrss/'><channel><title>" + UCase(video.network) + "</title><item><title>" + video.title + "</title><guid>" + video.programID + "</guid></item></channel></rss>"

    resource = url_encode( resource )

    if m.aetn_context <> invalid and m.aetn_context.adobePass <> invalid
        url = m.aetn_context.adobePass.adobePassEndPoint + "/api/v1/authorize?format=json&requestor=" + m.aetn_context.adobePass.requestor_id + "&deviceId=" + getDeviceESN() + "&resource=" + resource

        auth = getAuthorizationHeader(m.aetn_context.adobePass.public_key, m.aetn_context.adobePass.private_key, "GET", m.aetn_context.adobePass.requestor_id, "/api/v1/authorize")

        headers = [
            {
                name : "Authorization",
                value : auth
            }
        ]

        aetn_log("trace", "appSignIn : getAuthorization", "Authorization", auth)
        aetn_log("trace", "appSignIn: getAuthorization", "getAuthorization url", url)

        token = aetn_remote_get(url, false, headers)

        if token <> ""
            return ParseJSON(token)
        else
            return false
        end if
    else
        return false
    end if

End Function


Function getShortMediaToken( video )

    aetn_log("info", "appSignIn", "Get Short media token")

    resource = "<rss version='2.0' xmlns:media='http://search.yahoo.com/mrss/'><channel><title>" + UCase(video.network) + "</title><item><title>" + video.title + "</title><guid>" + video.programID + "</guid></item></channel></rss>"

    resource = url_encode( resource )

    if m.aetn_context <> invalid and m.aetn_context.adobePass <> invalid

        url = m.aetn_context.adobePass.adobePassEndPoint + "/api/v1/mediatoken?format=json&requestor=" + m.aetn_context.adobePass.requestor_id + "&deviceId=" + getDeviceESN() + "&resource=" + resource

        auth = getAuthorizationHeader(m.aetn_context.adobePass.public_key, m.aetn_context.adobePass.private_key, "GET", m.aetn_context.adobePass.requestor_id, "/api/v1/mediatoken")

        headers = [
            {
                name : "Authorization",
                value : auth
            }
        ]

        aetn_log("trace", "appSignIn : getShortMediaToken", "Auth", auth)
        aetn_log("trace", "appSignIn : getShortMediaToken", "Auth url", url)

        token = aetn_remote_get(url, false, headers)

        if token <> ""
            return ParseJSON(token)
        else
            return false
        end if
    else
        return false
    end if

End Function


Function getNewRegistrationCode()
    aetn_log("info", "appSignIn", "Get NewRegistrationCode")

    url     = m.aetn_context.adobePass.adobePassEndPoint + "/reggie/v1/" + m.aetn_context.adobePass.requestor_id + "/regcode"
    
    payload = "format=json&ttl=36000&deviceId=" + getDeviceESN()

    if m.aetn_context <> invalid and m.aetn_context.adobePass <> invalid

        auth    = getAuthorizationHeader(m.aetn_context.adobePass.public_key, m.aetn_context.adobePass.private_key, "POST", m.aetn_context.adobePass.requestor_id, "/reggie/v1/" + m.aetn_context.adobePass.requestor_id + "/regcode")

        aetn_log("trace", "appSignIn : getNewRegistrationCode", "url", url)
        aetn_log("trace", "appSignIn : getNewRegistrationCode", "payload", payload)
        aetn_log("trace", "appSignIn : getNewRegistrationCode", "auth", auth)

        headers = [
            {
                name : "Authorization",
                value : auth
            }
        ]

        data = aetn_remote_post( url, payload, headers )

        aetn_log("trace", "appSignIn : getNewRegistrationCode", "Get Registration Code", data, "registration code")

        if not data = ""
            data = ParseJSON(data)
            return data.code
        else
            aetn_error("Error", "Unable to generate registration code. Please try again later.")
            return ""
        end if
    else 
        aetn_error("Error", "Unable to generate registration code. Please try again later.")
        return ""
    end if

End Function


Function getDeviceESN()
    adobePassID = get_registry_section(m.aetn_context.rokulocalstorage.section, m.aetn_context.rokulocalstorage.adobepasskey)
    if adobePassID = Invalid
        adobePassID = CreateObject("roDeviceInfo").GetDeviceUniqueId() + itostr(Rnd(10000))
        set_registry_section(m.aetn_context.rokulocalstorage.section, m.aetn_context.rokulocalstorage.adobepasskey, adobePassID)
        set_registry_flush(m.aetn_context.rokulocalstorage.section)
    end if
    return adobePassID
End Function


Function getAuthorizationHeader( public_key, private_key, verb, requestor_id, request_uri )

    date            = CreateObject("roDateTime")
    current_time_f# = date.AsSeconds()
    thousand_f#     = 1000
    request_time_f# = current_time_f# * 1000
    request_time    = DoubleToString(request_time_f#)
    nonce           = generateUUID()

    msg = verb + " requestor_id=" + requestor_id + ", nonce=" + nonce + ", signature_method=HMAC-SHA1, request_time=" + request_time + ", request_uri=" + request_uri

    hmac = CreateObject("roHMAC") 
    
    signature_key = CreateObject("roByteArray") 
    signature_key.fromAsciiString(private_key)
    
    if hmac.setup("sha1", signature_key) = 0 

        message = CreateObject("roByteArray") 
        message.fromAsciiString(msg) 
        result = hmac.process(message) 
        
        return msg + ", public_key=" + public_key + ", signature=" + result.toBase64String() 

    end if

    return ""

End Function


Function generateUUID()

    uuid = ""
    
    for i=1 to 32
        o = RND(16)
        if o <= 10
            o = o + 47
        else
            o = o + 96 - 10
        end if
        uuid = uuid + CHR(o)
    end for

    return uuid

End Function


Function DoubleToString(x# as Double) as String

   onemill# = 1000000
   xhi = Int(x#/onemill#).toStr()
   xlo = Int((x#-onemill#*Int(x#/onemill#))).toStr()
   xlo = String(6-xlo.Len(),"0") + xlo

   return xhi+xlo

End Function