Function preVideoScreen()

    port = CreateObject("roMessagePort")

    screen = CreateObject("roSpringboardScreen")
    screen.SetMessagePort(port)
    screen.SetBreadcrumbText("Video", "")
    screen.SetDisplayMode("scale-to-fill")
    screen.SetPosterStyle("rounded-rect-16x9-generic")

    return screen

End Function


Function showVideoScreen(screen As Object, origVideo As Object) As Integer

    if validateParam(screen, "roSpringboardScreen", "showVideoScreen") = false return -1

    aetn_log("info", "appVideo", "Show Video Screen")

    if isnonemptystr(origVideo.AETNContentID)
        video = fetchContent(tostr(origVideo.AETNContentID), "video")
    end if

    if video = Invalid OR video.AETNContentID = ""
        return -1
    end if

    video.originalTitle = video.Title

    if origVideo.topicId  <> invalid
        video.topicId = origVideo.topicId
    end if

    if video.season = Invalid
        video.season = "0"
    end if

    if video.episode = Invalid
        video.episode = "0"
    end if

    if (video.season <> Invalid and toStr(video.season) <> "0") and (video.episode <> Invalid and toStr(video.season) <> "0")
        video.ShortDescriptionLine1 = video.title + " S" + toStr(video.season) + ":E" + toStr(video.episode)
    else if video.season <> Invalid and toStr(video.season) <> "0"
        video.ShortDescriptionLine1 = video.title + " S" + toStr(video.season)
    else 
        video.ShortDescriptionLine1 = video.title
    end if

    video.ShortDescriptionLine2 = video.description
    video.HDPosterUrl = video.thumbnailImageURL
    video.SDPosterUrl = video.thumbnailImageURL
    video.ContentType = "episode"
    video.AETNContentID = video.thePlatformId
'print "+++++++++++++";video
    
    if isnonemptystr(video.rating)
        video.Rating = UCase(video.rating)
    else
        video.Rating = ""
    end if

    video.releaseDate = video.airDate

    if isnonemptystr(video.isCloseCaption) AND video.isCloseCaption = "true"
        video.Rating = video.Rating + " | CC"
    end if

    if isnonemptystr(video.seriesName) and video.aetncontenttype <> "movie"

        video.originalTitle = video.Title
        
        if (video.season <> Invalid and toStr(video.season) <> "0") and (video.episode <> Invalid and toStr(video.season) <> "0")
            video.Title = video.Title + " S" + toStr(video.season) + ":E" + toStr(video.episode)
        else if (video.season <> Invalid and toStr(video.season) <> "0")
            video.Title = video.Title + " S" + toStr(video.season)
        end if

        screen.SetBreadcrumbText("Video", video.seriesName)
    end if 

    if video.totalVideoDuration <> Invalid
        video.length = Int( Int(video.totalVideoDuration) / 1000 )
    else
        video.length = 0
    end if
    
    if video.expirationDate <> Invalid
        date = strSplit(video.expirationDate, "T")
        video.Categories = CreateObject("roArray", 1, true)
        video.Categories.push("Available Until " + date[0])
    end if

    video.Description = video.description
    video.ContentType = "episode"

    if isnonemptystr(video.seriesName) and video.aetncontenttype <> "movie"
        seriesToAddObj = fetchContent(video.seriesName, "show", false)

        if seriesToAddObj <> Invalid
            seriesToAdd = seriesToAddObj
        end if
    end if

    screen.SetContent(video)
    screen.SetStaticRatingEnabled(false)

    buttons = []

    if strtobool(video.isBehindWall)
        if m.aetn_token <> Invalid
            if isPresentInContinueWatching(video)
                buttons.push("Resume Playing")
                buttons.push("Play From Beginning")
            else
                buttons.push("Play")  
            end if               
        else
            buttons.push("Sign In To Watch")
        end if
    else
        if isPresentInContinueWatching(video)
            buttons.push("Resume Playing")
            buttons.push("Play From Beginning")
        else
            buttons.push("Play")
        end if
        
    end if

    if isPresentInWatchlist(video)
        buttons.push("Remove from Watchlist")
    else
        buttons.push("Add to Watchlist")
    end if

    if isnonemptystr(video.seriesName) and video.aetncontenttype <> "movie"
        if seriesToAddObj <> Invalid 
            if isPresentInWatchlist(seriesToAdd)
                buttons.push("Remove Series from Watchlist")
            else
                buttons.push("Add Series to Watchlist")
            end if
        end if
    end if

    if video.aetncontenttype <> "movie"
        buttons.push("Browse All Videos")
    end if

    index = 0
    for each button in buttons
        screen.AddButton(index, button)
        index = index + 1
    next
    
    screen.Show()

    screenDetails = {}
    screenDetails.pageName = strRemoveSpace(video.Title)
    aetnOmniture("Video", screenDetails)

    while true
        msg = wait(0, screen.GetMessagePort())

        if type(msg) = "roSpringboardScreenEvent"

            aetn_log("info", "appVideo", "showVideoScreen | msg = " + msg.GetMessage() + " | index = " + itoStr(msg.GetIndex()) + " | column = " + itoStr(msg.GetData()))
            if msg.isButtonPressed()
                
                button = buttons[ msg.GetIndex() ]

                if button = "Resume Playing"
                    video.origAETNContentType = video.AETNContentType
                    video.AETNContentType = "player"
                    video["lastPlaybackPosition"] = lastPlaybackPositionInContinueWatching(video)
                    aetn_show_screen(video)
                    video.AETNContentType = video.origAETNContentType

                else if button = "Play" or button = "Play From Beginning"
                    video.origAETNContentType = video.AETNContentType
                    video.AETNContentType = "player"
                    video["lastPlaybackPosition"] = 0
                    aetn_show_screen(video)
                    video.AETNContentType = video.origAETNContentType

                else if button = "Sign In To Watch"
                    signin = {
                        Title: "Sign In",
                        AETNContentType: "signin"
                    }
                    aetn_show_screen(signin)

                    if m.aetn_token <> Invalid
                        buttons[ msg.GetIndex() ] = "Play"
                        screen.ClearButtons()
                        
                        index = 0
                        for each button in buttons
                            screen.AddButton(index, button) 
                            index = index + 1
                        next
                     
                        origAETNContentType = video.AETNContentType
                        video.AETNContentType = "player"

                        if isPresentInContinueWatching(video)
                            video["lastPlaybackPosition"] = lastPlaybackPositionInContinueWatching(video)
                        else
                            video["lastPlaybackPosition"] = 0
                        end if

                        aetn_show_screen(video)
                        video.AETNContentType = origAETNContentType

                    end if
                                        
                else if button = "Add to Watchlist"
                    addToWatchList(video)
                    
                    buttons[ msg.GetIndex() ] = "Remove from Watchlist"
                    screen.ClearButtons()
                    index = 0
                    for each button in buttons
                        screen.AddButton(index, button)
                        index = index + 1
                    next
                else if button = "Remove from Watchlist"
                    deleteFromWatchList(video)
                    buttons[ msg.GetIndex() ] = "Add to Watchlist"
                    screen.ClearButtons()
                    index = 0
                    for each button in buttons
                        screen.AddButton(index, button)
                        index = index + 1
                    next
                else if button = "Add Series to Watchlist"
                    addToWatchList(seriesToAdd)

                    buttons[ msg.GetIndex() ] = "Remove Series from Watchlist"
                    screen.ClearButtons()
                    index = 0
                    for each button in buttons
                        screen.AddButton(index, button)
                        index = index + 1
                    next
                else if button = "Remove Series from Watchlist"
                    deleteFromWatchList(seriesToAdd)

                    buttons[ msg.GetIndex() ] = "Add Series to Watchlist"
                    screen.ClearButtons()
                    index = 0
                    for each button in buttons
                        screen.AddButton(index, button)
                        index = index + 1
                    next
                else if button = "Browse All Videos"
                    if video.seriesName <> Invalid
                        showContent = {}
                        showContent.AETNContentID = video.seriesName
                        showContent.AETNContentType = "show"
                        aetn_show_screen(showContent)

                    else if video.network = "History" and video.topicId <> invalid
                        topicContent = {}
                        topicContent.AETNContentID = video.topicId
                        topicContent.AETNContentType = "topic"
                        aetn_show_screen(topicContent)
                    end if
                end if

            else if msg.isScreenClosed()
                exit while
            end if

            if buttons[0] = "Resume Playing" and not isPresentInContinueWatching(video)
                buttons.Shift()
                buttons[0] = "Play"
            end if
            
            if strtobool(video.isBehindWall) <> true or m.aetn_token <> Invalid
                if buttons[0] <> "Resume Playing" and isPresentInContinueWatching(video)
                    buttons.Unshift("Resume Playing")
                    buttons[1] = "Play From Beginning"
                end if
            end if

            screen.ClearButtons()
            index = 0
            for each button in buttons
                screen.AddButton(index, button) 
                index = index + 1
            next

        end if

    end while

    return 0

End Function