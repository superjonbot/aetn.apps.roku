Function aetnKrux(callType as String, screenName as String, status as String, callDetails = Invalid)

    ctx = {}
    kruxURL = "https://beacon.krxd.net/"

    if callType = "screen"

        url = kruxURL + "pixel.gif?"
        doScreenCall(ctx, screenName, callDetails)

    else if callType = "event"

        url = kruxURL + "event.gif?"
        doEventCall(ctx, status, callDetails)

    else if callType = "heartbeat"

        url = kruxURL + "heartbeat.gif?"
        doEventCall(ctx, status, callDetails)

    end if
    
    ctx._kcp_d = m.aetn_context.omniture.name + "_Roku"
    
    if m.aetn_context.omniture.name = "A&E"
        ctx._kcp_d = "AETV_Roku"
    end if
    
    ctx._kcp_s = ctx._kcp_d
    ctx._kuid = getDeviceESN()
    ctx._kpid = "7156d277-5d35-4c9c-8fb7-f454c47dbfe1"
    checkAuthentication(ctx)

    sendKruxRequest(url, ctx)

End Function



Function doScreenCall( ctx as Object, screenName As String, screenDetails = Invalid)

    if screenDetails <> Invalid
        if isnonemptystr(screenDetails.pageName) 
            ctx._kpa_page_level_2 = screenDetails.pageName
        end if
        if screenDetails.network = "H2"
            screenName = "H2Shows"
        end if
        if screenName = "Search"
            ctx._kpa_internal_search_manual = screenDetails.searchTerm
            ctx.event_id = "JYdmH5gc"
        end if
    end if

    ctx._kcp_sc = screenName
    ctx._kpa_page_level_1 = screenName

End Function


Function doEventCall( ctx as Object, status as String, videoDetails = Invalid)
    
    if status = "Start"
        ctx.event_id = "JLn4isGR"
    else if status = "End"
        ctx.event_id = "JLn4ukRD"
    end if

    if videoDetails.longForm = "true"
        ctx._kpa_video_analytics_lf_vs_sf = "Longform"
    else
        ctx._kpa_video_analytics_lf_vs_sf = "Shortform"
    end if

    if toStr(videoDetails.chapter) <> "PrerollAd" and toStr(videoDetails.chapter) <> "None"
        ctx._kpa_video_chapter = "Chapter " + toStr(videoDetails.chapter)
    end if

    if videoDetails.Episode <> Invalid
        ctx._kpa_video_episode = "Episode " + toStr(videoDetails.episode)
    end if

    ctx._kpa_video_analytics_clip_title = toStr(videoDetails.brandName) + ":" + toStr(videoDetails.omnitureTag) + ":" + toStr(videoDetails.originalTitle)
    ctx._kpa_video_analytics_series_name = toStr(videoDetails.omnitureTag)
    ctx._kpa_video_season = "Season " + toStr(videoDetails.season)
    ctx._kpa_video_series_type = videoDetails.tvNtvMix
    ctx._kpa_video_PPLID = toStr(videoDetails.programID)

    duration = roundInt((videoDetails.totalVideoDuration / 1000) / 60, 1)
    ctx._kpa_video_duration = strTrim( toStr(duration) )
    
End Function



Function checkAuthentication(ctx As Object)

    if m.aetn_token <> Invalid
        ctx._kpa_is_tve_authenticated = "1"
        ctx._kpa_tve_provider_name = m.aetn_token.mvpd
    else
        ctx._kpa_is_tve_authenticated = "0"
    end if
    
End Function



Function sendKruxRequest(url As String, ctx As Object)

    http = CreateObject("roUrlTransfer")
    http.SetPort(CreateObject("roMessagePort"))

    For Each i in ctx
        url = url + i + "=" + http.escape(ctx[i]) + "&"
    End For

    http.setUrl(url)
    aetn_log("debug", "Krux", "Send Krux Request: " + url)
    
    http.getToString()

End Function

