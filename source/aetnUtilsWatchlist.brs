Function addToWatchList(videoObj as Dynamic) as Boolean
    
    aetn_log("info", "aetnUtil", "Registry Object")

    currentWatchListStr = get_registry_section(m.aetn_context.rokulocalstorage.section, m.aetn_context.rokulocalstorage.watchlistkey)

    if currentWatchListStr = Invalid
        currentWatchList = []
    else
        currentWatchList = ParseJSON(currentWatchListStr)

        index = 0
        for each item in currentWatchList
            if item.AETNContentID = videoObj.AETNContentID
                currentWatchList.Delete(index)
                index = index - 1   
            end if 
            index = index + 1
        next
    end if

    if type(currentWatchList) <> "roArray"
        aetn_error("There was an error retrieving your Watch List")
        'Delete local json
        currentWatchList = []
    end if

    if currentWatchList.Count() >= 20
        currentWatchList.Pop()
    end if

    'serialize watchlist object
    watchListObj = {}

    watchListObj.Title = videoObj.title
    watchListObj.HDPosterUrl = videoObj.HDPosterUrl
    watchListObj.SDPosterUrl = videoObj.SDPosterUrl
    watchListObj.ContentType = videoObj.ContentType
    watchListObj.AETNContentType = videoObj.AETNContentType
    watchListObj.AETNContentID = videoObj.AETNContentID
    watchListObj.description = videoObj.description

    currentWatchList.Unshift(watchListObj)

    set_registry_section(m.aetn_context.rokulocalstorage.section, m.aetn_context.rokulocalstorage.watchlistkey, currentWatchList)

    set_registry_flush(m.aetn_context.rokulocalstorage.section)

    m.refreshHomeScreen = true
    
    return true
    
End Function


Function deleteFromWatchList(videoObj as Dynamic) as Boolean
    
    currentWatchListStr = get_registry_section(m.aetn_context.rokulocalstorage.section, m.aetn_context.rokulocalstorage.watchlistkey)

    if currentWatchListStr = Invalid
        return false
    else
        currentWatchList = ParseJSON(currentWatchListStr)
        index = 0
        for each item in currentWatchList
            if item.AETNContentID = videoObj.AETNContentID
                aetn_log("debug", "aetnUtilsWatchlist", "item printed after match", item, "Item to be deleted")
                currentWatchList.Delete(index)
                index = index - 1
            end if 
            index = index + 1
        next       
    end if

    set_registry_section(m.aetn_context.rokulocalstorage.section, m.aetn_context.rokulocalstorage.watchlistkey, currentWatchList)
    set_registry_flush(m.aetn_context.rokulocalstorage.section)
    
    m.refreshHomeScreen = true

    return true

End Function


Function isPresentInWatchlist(videoObj as Dynamic) as Boolean
    
    currentWatchListStr = get_registry_section(m.aetn_context.rokulocalstorage.section, m.aetn_context.rokulocalstorage.watchlistkey)

    if currentWatchListStr <> Invalid
        currentWatchList = ParseJSON(currentWatchListStr)
        for each item in currentWatchList
            if item.AETNContentID = videoObj.AETNContentID
                return true
            end if 
        next       
    end if
    
    return false

End Function