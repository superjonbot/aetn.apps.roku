Function preWatchlistScreen()

    port = CreateObject("roMessagePort")

    screen = CreateObject("roPosterScreen")
    screen.SetMessagePort(port)
    screen.SetBreadcrumbText("", "Watchlist")
    screen.SetDisplayMode("scale-to-fill")
    screen.SetListStyle("arced-16x9")

    return screen

End Function


Function showWatchlistScreen(screen As Object, contentObj As Object) As Integer

    if validateParam(screen, "roPosterScreen", "showShowsScreen") = false return -1

    aetn_log("info", "appWatchlist", "Show Watchlist Screen")

    screen.Show()
    aetnOmniture("Watchlist")
    contentLists = setWatchlistScreen(screen, contentObj)

    if contentLists = Invalid
        return 0
    end if
    
    while true
        msg = wait(0, screen.GetMessagePort())
        if type(msg) = "roPosterScreenEvent"
            aetn_log("info", "appWatchlist", "showShowsScreen | msg = " + msg.GetMessage() + " | index = " + itoStr(msg.GetIndex()) + " | column = " + itoStr(msg.getData()))
            if msg.isListItemSelected()
                index = msg.GetIndex()
                if contentLists[index].contenttype <> "Back"
                    aetn_show_screen(contentLists[index])
                end if
            else if msg.isScreenClosed()
                return -1
            end if
        end if
    end while

    return 0

End Function

Function setWatchlistScreen(screen As Object, contentObj As Object)

    parsedWatchlist = [{
        ShortDescriptionLine1 : "Videos you've added will appear here",
        ShortDescriptionLine2 : "Go Back",
        hdposterurl : "",
        sdposterurl : "",
        contenttype : "Back"
    }]

    index = 0

    watchlistStr = get_registry_section(m.aetn_context.rokulocalstorage.section, m.aetn_context.rokulocalstorage.watchlistkey)    
    
    if watchlistStr = Invalid
        aetn_error("Watch List", "No Items found")
        return Invalid
    end if

    aetn_log("trace", "appWatchlist : setWatchlistScreen", "List Str", watchlistStr)
    parsedWatchlist = ParseJSON(watchlistStr)

    if parsedWatchlist = Invalid
        aetn_error("Watch List", "No Items found")
        return Invalid
    end if 

    aetn_log("trace", "appWatchlist : setWatchlistScreen", "Parsed List", parsedWatchlist)
    if parsedWatchlist.count() = 0
        aetn_error("Watch List", "No Items found")
        return Invalid
    end if

    for each item in parsedWatchlist
        item.ShortDescriptionLine1 = item.title
        item.ShortDescriptionLine2 = item.description
    next

    screen.SetContentList(parsedWatchlist)
    return parsedWatchlist

End Function