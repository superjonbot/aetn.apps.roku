Sub Main(args as Dynamic)

    screenFacade = CreateObject("roPosterScreen")
    screenFacade.show()
    
    appSetup()
    
    if args <> Invalid and args["url"] <> Invalid  
        handleDeepLinking(args)
    end if
    m.juststarted = true

    screen = preHomeScreen()
    showHomeScreen(screen)

    ' screenFacade.showMessage("")
    ' sleep(5)
End Sub

Function appSetup()
    
    initTheme()
    aetn_setGlobalContext(m.appConfig["configUrl"])

End Function


' Grab settings from config on server
Sub initTheme()

    app = CreateObject("roAppManager")

    theme = CreateObject("roAssociativeArray")
    themeJSONStr = ReadAsciiFile("pkg:/theme.json")
    themeProps = ParseJSON(themeJSONStr)
    if themeProps <> Invalid
        for each item in themeProps
            if toStr(themeProps[item]) <> ""
                theme[item] = themeProps[item]
            end if
        end for
    end if
    app.SetTheme(theme)

    m.appConfig = CreateObject("roAssociativeArray") 
    appConfigJSONStr = ReadAsciiFile("pkg:/app_config.json")
    appConfig = ParseJSON(appConfigJSONStr)
    if appConfig <> Invalid
        for each item in appConfig
            if toStr(appConfig[item]) <> ""
                m.appConfig[item] = appConfig[item]
            end if
        end for
    end if

End Sub

Sub setMVPDLogo(mvpd as String)
    aetn_log("info", "appMain", "Set MVPD Logo. Provider: " + mvpd)
    app = CreateObject("roAppManager")

    theme = CreateObject("roAssociativeArray")
    
    fs = CreateObject( "roFileSystem" )
    if fs.Exists("pkg:/images/" + mvpd + "-logo-hd.png")
        theme["OverhangPrimaryLogoHD"] = "pkg:/images/" + mvpd + "-logo-hd.png"
        theme["GridScreenLogoHD"] = "pkg:/images/" + mvpd + "-logo-hd.png"
    end if

    if fs.Exists("pkg:/images/" + mvpd + "-logo-sd.png")
        theme["OverhangPrimaryLogoSD"] = "pkg:/images/" + mvpd + "-logo-sd.png"      
        theme["GridScreenLogoSD"] = "pkg:/images/" + mvpd + "-logo-sd.png"
    end if

    app.SetTheme(theme)

End Sub

Sub removeMVPDLogo()
    aetn_log("info", "appMain", "Remove MVPD logo")
    app = CreateObject("roAppManager")

    theme = CreateObject("roAssociativeArray")
    
    theme["OverhangPrimaryLogoHD"] = "pkg:/images/overhang-primary-logo-hd.png"
    theme["OverhangPrimaryLogoSD"] = "pkg:/images/overhang-primary-logo-sd.png"
    
    theme["GridScreenLogoHD"] = "pkg:/images/overhang-primary-logo-hd.png"
    theme["GridScreenLogoSD"] = "pkg:/images/overhang-primary-logo-sd.png"
    
    app.SetTheme(theme)

End Sub
