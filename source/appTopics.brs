Function preTopicsScreen()

    port = CreateObject("roMessagePort")

    screen = CreateObject("roPosterScreen")
    screen.SetMessagePort(port)
    screen.SetBreadcrumbText("Home", "Topics")
    screen.SetDisplayMode("scale-to-fill")
    screen.SetListStyle("arced-16x9")

    return screen

End Function

Function showTopicsScreen(screen As Object, contentObj as Object) As Integer

    if validateParam(screen, "roPosterScreen", "showTopicsScreen") = false return -1

    contentObj = fetchContent(contentObj.AETNContentID, "topic")
    if contentObj = Invalid
        return -1
    end if

    aetn_log("info", "appTopic", "Show Topic Screen")

    screen.SetBreadcrumbText("Topic", contentObj.title)

    screen.Show()
    screenDetails = {}
    screenDetails.pageName = strRemoveSpace(contentObj.title)
    aetnOmniture("Topics", screenDetails)
    aetnKrux("screen", "Topics", "", screenDetails)
    topicArray = contentObj

    if topicArray = Invalid or topicArray.videos = Invalid
        aetn_error("Error", "No Topic Matching that title found")
    end if

    contentList = topicArray.videos
    screen.SetContentList(contentList)

    while true
        
        msg = wait(0, screen.GetMessagePort())
        
        if type(msg) = "roPosterScreenEvent"

            aetn_log("info", "appTopics", "showTopicsScreen | msg = " + msg.GetMessage() + " | index = " + itoStr(msg.GetIndex()) + " | column = " + itoStr(msg.getData()))

            if msg.isListItemSelected()

                index = msg.GetIndex()
                contentList[index].topicID = contentObj.AETNContentID
                aetn_show_screen(contentList[index])
            
            else if msg.isScreenClosed()
            
                return -1
            
            end if
        
        end if
    
    end while

    return 0

End Function