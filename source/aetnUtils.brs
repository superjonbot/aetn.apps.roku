Function aetn_loading(title, text="")

    if not isstr(title) title = ""
    if not isstr(text) text = ""

    port = CreateObject("roMessagePort")
    dialog = Invalid

    if text = ""
        dialog = CreateObject("roOneLineDialog")
    else
        dialog = CreateObject("roMessageDialog")
        dialog.SetText(text)
    end if

    dialog.SetMessagePort(port)
    dialog.SetTitle(title)
    dialog.ShowBusyAnimation()
    dialog.Show()

    return dialog

End Function


Function aetn_setGlobalContext(contextUrl)

    dialog = aetn_loading("Loading...", "")
    
    context = aetn_remote_get(contextUrl)
    m.aetn_context = ParseJSON(context)
    
    if (m.aetn_context.mvpdURL <> invalid) 
        mapping = aetn_remote_get(m.aetn_context.mvpdURL)
        result = ParseJSON(mapping)
        m.mvpdFWMap = {}
        if result <> invalid
            for each item in result.mvpdWhitelist
                if item.freewheelKeyValues <> invalid
                    m.mvpdFWMap[item.mvpd] = item.freewheelKeyValues.mvpdHash
                end if
            end for
        end if
        printAnyAA(2, m.mvpdFWMap)
    end if
    

    m.aetn_token = getAuthenticationToken()

    if m.aetn_token <> Invalid
        setMVPDLogo(m.aetn_token.mvpd)
    end if

    dialog.Close()

End Function

Function aetn_error(title, text="", buttonName = Invalid) As Void 
    
    if not isstr(title) title = ""
    if not isstr(text) text = ""
    if not isStr(buttonName) buttonName = "OK"

    port = CreateObject("roMessagePort")
    dialog = CreateObject("roMessageDialog")
    dialog.SetMessagePort(port) 
    dialog.SetTitle(title)
    dialog.SetText(text) 
    dialog.AddButton(0, buttonName)
    dialog.EnableBackButton(true)
    dialog.Show()
    
    while True
        dlgMsg = wait(0, dialog.GetMessagePort())
        if type(dlgMsg) = "roMessageDialogEvent"
            if dlgMsg.isButtonPressed()
                dialog.Close()
            else if dlgMsg.isScreenClosed()
                exit while
            end if
        end if
    end while

End Function


Function aetn_message(title, text="", buttonName = Invalid)

    if not isstr(title) title = ""
    if not isstr(text) text = ""
    if not isStr(buttonName) buttonName = "OK"

    port = CreateObject("roMessagePort")
    dialog = Invalid

    dialog = CreateObject("roMessageDialog")
    dialog.SetText(text)
    dialog.SetMessagePort(port)
    dialog.SetTitle(title)
    dialog.AddButton(1, buttonName)
    dialog.Show()

    dialog.EnableBackButton(true)
    
    while true
        msg = wait(0, port)
        if type(msg) = "roMessageDialogEvent"
            if msg.isButtonPressed()
                exit while
            else if msg.isScreenClosed()
                print "aetn_message back"
                exit while
            end if
        end if
    end while

End Function

Function aetn_remote_get(url, async=false, headers=[], addDeviceId=false)

    if not isstr(url) return ""

    if addDeviceId
        if Instr(1, url, "deviceId") = 0
            if Instr(1, url, "?") > 0
                url = url + "&deviceId=roku"
            else 
                url = url + "?deviceId=roku"
            end if
        end if
    end if

    aetn_log("trace", "aetnUtils: aetn_remote_get async=" + AnyToString(async), "Remote Get:" + url)
    port = CreateObject("roMessagePort")
    http = CreateObject("roUrlTransfer")
    http.SetMessagePort(port)
    http.SetUrl(url)
    http.EnableEncodings(true)
    http.EnableFreshConnection(true)
    http.EnableCookies()
    http.SetCertificatesFile("common:/certs/ca-bundle.crt")
    http.InitClientCertificates()

    for each header in headers
        http.AddHeader(header.name, header.value)
    next

    if async
        'result = http.AsyncGetToString()
        'return result
        if http.AsyncGetToString()
            
            while (true)
                msg = wait(0, port)
                if (type(msg) = "roUrlEvent")
                    code = msg.GetResponseCode()
                    'header = msg.getResponseHeaders()
                    'printAA(header)                    
                    if (code = 200)
                        return msg.GetString()   
                    else
                        print "Failure reason: ";msg.GetFailureReason()                    
                    endif
                else if (event = invalid)
                    request.AsyncCancel()

                endif
            end while
        endif
        return ""
        
    else 
        return http.GetToString()    
    end if

End Function


Function aetn_remote_post(url, data, headers=[])

    port = CreateObject("roMessagePort")

    http = CreateObject("roUrlTransfer")
    http.SetPort(port)
    http.SetUrl(url)
    http.EnableEncodings(true)
    http.EnableFreshConnection(true)
    http.SetCertificatesFile("common:/certs/ca-bundle.crt")
    http.InitClientCertificates()
    http.AddHeader("Content-Type", "application/x-www-form-urlencoded")

    for each header in headers
        http.AddHeader(header.name, header.value)
    next

    if http.AsyncPostFromString(data)
        
        msg = wait(0, port)
        
        if type(msg) = "roUrlEvent"

            code = msg.GetResponseCode()            
            return msg.GetString()
        
        elseif msg = Invalid
            aetn_error("Error", "Unable to connect to server")
            http.AsyncCancel()

        else
            aetn_error("Error", "Unable to connect to server")
        end if
    end if

    return ""
End Function

Function aetn_remote_delete(url, async=false, headers=[])

    if not isstr(url) return -1

    port = CreateObject("roMessagePort")
    http = CreateObject("roUrlTransfer")
    http.SetMessagePort(port)
    http.SetUrl(url)
    http.SetRequest("DELETE")
    http.EnableEncodings(true)
    http.EnableFreshConnection(true)
    http.SetCertificatesFile("common:/certs/ca-bundle.crt")
    http.InitClientCertificates()
    
    for each header in headers
        http.AddHeader(header.name, header.value)
    next

    if async
        http.AsyncGetToString()
        return port
    else
        return http.GetToString()
    end if

End Function

Function aetn_log(logType As String, logSource, logMessage, logObject = Invalid, objectLabel = Invalid)
    logLevel = getLogLevel(logType)
    if logLevel >= getLogLevel(m.appConfig["logLevel"])
        date = CreateObject("roDateTime")
        print date.AsDateString("short-date");date.GetHours();date.getMinutes();date.getSeconds();" ";logSource; " "; UCase(logType); " => "; logMessage
        if logLevel <= 2 and  logObject <> Invalid
            if objectLabel = Invalid
                objectLabel = logMessage + " Object"
            end if
            print "        ************BEGIN ";objectLabel;"*************"
            printAny(4, "", logObject)
            print "        *************END ";objectLabel;"************"
        end if
    end if

End Function

Function getLogLevel(logType ) as Integer
    'logType = LCase(logType)
    if logType = "trace"
        return 1
    else if logType = "debug"
        return 2
    else if logType = "info"
        return 3
    else if logType = "warn"
        return 4
    else if logType = "error"
        return 5
    else
        return 0
    end if

End Function


Function url_encode(str As String) As String

    if not m.DoesExist("encodeProxyUrl") then m.encodeProxyUrl = CreateObject("roUrlTransfer")
    
    return m.encodeProxyUrl.Escape(str)

End Function


Function url_decode(str As String) As String

    if not m.DoesExist("encodeProxyUrl") then m.encodeProxyUrl = CreateObject("roUrlTransfer")
    
    return m.encodeProxyUrl.Unescape(str)

End Function

Function fetchVideos(contentIDs as String) as Dynamic
    videosJSON = aetn_remote_get(m.aetn_context.titleFeedURL + "episode/" + m.appConfig["wombatAppId"] + "?title_id=" + contentIDs, false, [], true)
    videoData = ParseJSON(videosJSON)
    videos = {}

    if videoData = Invalid OR videoData.Items = Invalid
            if raiseError = true
                aetn_error("Error in fetching Video")
            end if
            return Invalid 
    else
        For Each item In videoData.Items
            videos[item.thePlatformId] = item
        End For
        return videos

    end if


End Function


Function fetchContent(contentID as String, contentType as string, raiseError = true, network = Invalid) as Dynamic
    if contentType = "show"

        if network = "H2"
            showJSON = aetn_remote_get(m.aetn_context.h2ShowFeedURL + "&showName=" + url_encode(contentID), false, [], true)
        else
            showJSON = aetn_remote_get(m.aetn_context.showFeedURL + "&showName=" + url_encode(contentID), false, [], true)
        end if

        showData = ParseJSON(showJSON)
        
        if showData = Invalid OR showData.Count() = 0
            if raiseError = true
                aetn_error("Error in fetching Show")
            end if
            return Invalid
        else
            return showData[0]
        end if


    else if contentType = "video"

        videoJSON = aetn_remote_get(m.aetn_context.titleFeedURL + "episode/" + m.appConfig["wombatAppId"] + "?title_id=" + url_encode(contentID), false, [], true)
        videoData = ParseJSON(videoJSON)

        if videoData = Invalid OR videoData.Items = Invalid
            if raiseError = true
                aetn_error("Error in fetching Video")
            end if
            return Invalid 
        else
            if (videoData.Items[0].mrssLengthType = "Feature_Film")
                videoData.Items[0].AETNContentType = "movie"
            else 
                videoData.Items[0].AETNContentType = "video"
            end if
            return videoData.Items[0]
        end if

    else if contentType = "topic"
        topicJSON = aetn_remote_get(m.aetn_context.topicsFeedURL + "?topicName=" + url_encode(contentID), false, [], true)
        topicData = ParseJSON(topicJSON)

        if topicData = Invalid
            if raiseError = true
                aetn_error("Error in fetching Topic")
            end if
            return Invalid
        else
            return topicData[0]
        end if

    end if

End Function

' Function aetn_fetch_topic(topicTitle as String)

'     topicsJSON = aetn_remote_get(m.aetn_context.topicsFeedURL, false, [], true)
'     topicsArray = ParseJSON(topicsJSON)

'     if topicsArray = Invalid return Invalid

'     for each topic in topicsArray.items
'         if topic.title = topicTitle then return topic
'     next

'     return Invalid

' End Function


' Function fetchContent(contentID as String, contentType as string, network = Invalid) as Dynamic
'     result = fetchContentWithoutError(contentId, contentType, network)

'     if isStr(result)
'         aetn_error(result)
'         return Invalid
'     else
'         return result
'     end if

' End Function