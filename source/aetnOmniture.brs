Function aetnOmniture(screenName as String, screenDetails = Invalid, eventNames = Invalid)
    
    s = OmnitureMeasurement(m.aetn_context.omniture.suiteIds)


    s.trackingServer = m.aetn_context.omniture.server
    s.ssl = false
    s.debugTracking = true
    s.roku = CreateObject("roAssociativeArray")

    setupEvents(s, eventNames)

    if isnullorempty(screenName) = false and not screenName = "Authentication" and not screenName = "SearchResults"

        eventPageView(s, screenName, screenDetails)

        if isnullorempty(screenName) = false
            m.screenNameForOmin = screenName
        end if

    else
        if isnullorempty(screenName) 
            screenName = m.screenNameForOmin
        else 
            m.screenNameForOmin = screenName
        end if

        if screenName = "Player"
            screenDetails.brandName = m.aetn_context.omniture.brandName
            eventVideo(s, screenDetails)

        else if screenName = "SearchResults"
            eventSearch(s, screenDetails)

        else if screenName = "Authentication"
            eventAuthentication(s)

        else if screenName = "Tile"
            eventTileClick(s, screenDetails)
        end if
    end if

    s.roku.platform = "Roku"
    s.roku.brand = m.aetn_context.omniture.name

    sendRequest(s, s.roku)

End Function

Function setupEvents(s As Object, eventNames) 
    
    if eventNames <> Invalid and eventNames.count() > 0

        eventNumberStr = ""

        for each event in eventNames

            eventNumber = getEventNumber(event)

            if eventNumber <> Invalid
                eventNumberStr = eventNumberStr + "event" + toStr(eventNumber) + ","
            else
                aetn_error("Event Not Found", event + " was lookedup, but not found in the eventNumberMap")
                return -1
            end if

        next

        s.events = eventNumberStr

    end if

End Function

Function getEventNumber(eventName as String)

    eventNumberMap = {}
    eventNumberMap[ "Video View" ]        = 21
    eventNumberMap[ "Video Start" ]       = 22
    eventNumberMap[ "Video Complete" ]    = 26
    eventNumberMap[ "Episode Start" ]     = 27
    eventNumberMap[ "Episode Complete" ]  = 28
    eventNumberMap[ "Video Ad Start" ]    = 30
    eventNumberMap[ "Video Ad Complete" ] = 31
    eventNumberMap[ "Share" ]             = 2
    eventNumberMap[ "Internal Search" ]   = 7
    eventNumberMap[ "TV Everywhere Authentication Start" ]    = 72
    eventNumberMap[ "TV Everywhere Authentication Complete" ] = 73
    eventNumberMap[ "TV Everywhere Authentication Error" ]    = 76

    if eventNumberMap[eventName] <> Invalid
        return eventNumberMap[eventName]
    else
        aetn_error("Event Not Found", eventName + " was lookedup, but not found in the eventNumberMap")
        return Invalid
    end if

End Function


Function eventVideo(s As Object, videoDetails As Object)
    
    s.linkname = "RokuVideo"

    s.roku.videocalltype = toStr(videoDetails.videocalltype)
    s.roku.videocliptitle = toStr(videoDetails.brandName) + ":" + toStr(videoDetails.omnitureTag) + ":" + toStr(videoDetails.originalTitle)
    s.roku.videocategory = toStr(videoDetails.brandName) + ":" + toStr(videoDetails.omnitureTag)

    if videoDetails.longForm = "true"
        s.roku.videolf = "Longform"
    else
        s.roku.videolf = "Shortform"
    end if

    'video need to be authenticated or not
    if videoDetails.isBehindWall = "true"
        s.roku.videorequiresauthentication = "1"
    else
        s.roku.videorequiresauthentication = "0"
    end if

    if not videoDetails.programId = Invalid
        s.roku.pplid = videoDetails.programId
    end if
    
    s.roku.videoseason = "Season " + toStr(videoDetails.season)
    s.roku.videotv = "TV"

    if toStr(videoDetails.chapter) = "PrerollAd" or toStr(videoDetails.chapter) = "None"
        s.roku.videochapter = toStr(videoDetails.chapter)
    else
        s.roku.videochapter = "Chapter " + toStr(videoDetails.chapter)
    end if
    
    if videoDetails.Episode <> Invalid
        s.roku.videoepisode = "Episode " + toStr(videoDetails.episode)
    else 
        s.roku.videoepisode = ""
    end if

    s.roku.videosubcategory = "None"
    
    if videoDetails.isFastFollow = "true"
        s.roku.videofastfollow = "FastFollow"
    else 
        s.roku.videofastfollow = "Shortform"
    end if
    
    s.roku.videoautoplay = "Autoplay"
    duration = roundInt(videoDetails.totalVideoDuration / 1000, 5)
    s.roku.videoduration = strTrim( toStr(duration) )
    s.roku.videofullscreen = "Fullscreen"

    if videoDetails.videoadvertiser = ""
        videoDetails.videoadvertiser = "None"
    end if

    if toStr(videoDetails.videoadduration) = ""
        videoDetails.videoadduration = "None"
    end if

    if videoDetails.videoadvertiser = ""
        videoDetails.videoadvertiser = "None"
    end if

    if videoDetails.videoadtitle = ""
        videoDetails.videoadtitle = "None"
    end if

    s.roku.videoadvertiser = toStr(videoDetails.videoadvertiser)
    s.roku.videoadduration = toStr(videoDetails.videoadduration)
    s.roku.videoadtitle = toStr(videoDetails.videoadvertiser) + ":" + toStr(videoDetails.videoadtitle)


End Function


Function eventTileClick(s As Object, tile As Integer)
    
    s.roku.featureposition = tile

End Function


Function eventPageView(s As Object, screenName As String, screenDetails = Invalid)
    if screenDetails = Invalid 
        s.pagename = m.aetn_context.omniture.brandName + ":" + screenName
    else if isnonemptystr(screenDetails.pageName)
        s.pagename = m.aetn_context.omniture.brandName + ":"  + screenName + ":" + screenDetails.pageName
    end if

End Function


Function eventAuthentication(s As Object)

    s.linkname = "RokuUserAuthentication"

    if m.aetn_token <> Invalid
        s.roku.authenticationstate = "1"
        s.roku.cableprovidername = m.aetn_token.mvpd
    else
        s.roku.authenticationstate = "0"
        s.roku.cableprovidername = "None"
    end if
    
End Function


Function eventSearch(s as Object, screenDetails as Object)
    s.linkname = "RokuSearch"
    s.roku.internalsearchterm = screenDetails.searchTerm

End Function
