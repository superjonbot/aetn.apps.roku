Function prePlayerScreen()

    port = CreateObject("roMessagePort")

    screen = CreateObject("roImageCanvas")
    screen.SetLayer(0, "#000000")
    screen.SetMessagePort(port)

    return screen

End Function


Function showPlayerScreen(screen As Object, video As Object) As Integer

    if validateParam(screen, "roImageCanvas", "showPlayerScreen") = false return -1
    aetn_log("info", "appPlayer", "showPlayerScreen")
    screen.Show()
    screenDetails = {}

    if video.lastPlaybackPosition <> Invalid
        video_position = video.lastPlaybackPosition
    else
        video_position = 0
    end if

    screenDetails.pageName = strRemoveSpace(video.Title)
    aetnOmniture("Player", screenDetails)

    video.playURL_HLS = strReplace(video.playURL_HLS, "https://", "http://")
    
    if CreateObject("roDeviceInfo").GetDisplayType() = "HDTV"
        video.StreamQualities = ["HD"]
    else
        video.StreamQualities = ["SD"]
    end if

    video.StreamBitrates = [0]
    video.StreamFormat = "hls"
    video.SwitchingStrategy = "full-adaptation"
    video.ContentType = "episode"
    video.cuepoints = parseCuePoints(video)

    video.chapterTracker = []
    video.chapterTracker[0] = {checkpoint: 0, isStarted: false, fireCompleteEvent: false}

    For i = 0 To video.cuepoints.count() - 1 Step 1 
        video.chapterTracker.push({checkpoint: video.cuepoints[i].seconds, isStarted: false, fireCompleteEvent: false})
    End For

    adsWrapper = getAds(video)  
    ads = adsWrapper.slots
    
    if not ads = Invalid and not ads[ "0" ] = Invalid and not ads[ "0" ].played and video_position = 0
        video.chapter = "PrerollAd"

        'for pay video and when user is signed in, check the video authZ before play preroll ad
        if strtobool(video.isBehindWall) and m.aetn_token <> Invalid
            authz = getAuthorization( video )
            token = getShortMediaToken( video )
            
            if isbool(token)   
                aetn_error("Error", "User not authorized.")
                return -1
            end if
        end if

        result = playSlot("0", ads, video)
        if result = false
            screen.Close()
            return 0
        end if
    else
        ads = {}
    end if

    aetn_remote_get(adsWrapper.callbackUrl)

    video.chapter = 1
    while not video_position = -1
        'print "showPlayerScreen";video_position;
        video_position = playVideo(video, video_position, ads)
        if (video.longform = "true")
            video.chapter = video.chapter + 1
        end if
    end while

    screen.Close()
    return 0
End Function

Function EnableCaptions(player as object) as void
  di = CreateObject("roDeviceInfo")
  captionsMode = di.GetCaptionsMode()
 
  if (captionsMode = "On")
      player.ShowSubtitle(true)
      player.ShowSubtitleOnReplay(false)
  else if (captionsMode = "Off")
      player.ShowSubtitle(false)
      player.ShowSubtitleOnReplay(false)
  else if (captionsMode = "Instant replay")
      player.ShowSubtitle(false)
      player.ShowSubtitleOnReplay(true)
  end if
End Function


Function playVideo(video As Object, video_position As Integer, ads As Object) As Integer

    port = CreateObject("roMessagePort")

    addToContinueWatching(video, video_position)

    video.PlayStart = video_position

    stream_url = authorizeVideo(video)

    if stream_url = Invalid
        return -1
    end if

    video.SubtitleUrl = getCloseCaptionUrl(stream_url)

    video.StreamUrls = [stream_url]

    player = CreateObject("roVideoScreen")
    player.SetMessagePort(port)
    player.SetCertificatesFile("common:/certs/ca-bundle.crt")
    player.InitClientCertificates()
    player.SetPositionNotificationPeriod(2)

    player.SetContent(video)

    EnableCaptions(player)

    player.Show()
    video.videocalltype = "ContentView"

    if video.longform = "false"
        video.chapter = "None"
    end if
    
    scrubbedPastCuePoint = false
    
    previous_position = video_position
    fireEpisodeCompleteEvent = false
    scrubbed = false

    kruxHeartbeat = 0

    while true

        msg = wait(0, player.GetMessagePort())

        if type(msg) = "roVideoScreenEvent"

            if msg.isRequestFailed()
                aetn_log("error", "videoPlayer", "Video request failure: " + msg.GetMessage())
                aetn_error("Error", "Unable to play video.")

            else if msg.isStatusMessage()
                aetn_log("info", "videoPlayer", "Video status: " + msg.GetMessage())

            else if msg.isButtonPressed()
                aetn_log("info", "videoPlayer", "Button pressed: " + msg.GetIndex() + " " + msg.GetData())

            else if msg.isPlaybackPosition()
                aetn_log("debug", "videoPlayer", "Video current time: " + itoStr(msg.GetIndex()) + " of " + itoStr(video.totalVideoDuration/1000))

                if previous_position + 5 < msg.GetIndex()
                    scrubbed = true
                else
                    scrubbed = false
                end if

                'KRUX heartbeat
                if scrubbed = false
                    kruxHeartbeat = kruxHeartbeat + (msg.GetIndex() - previous_position)
                end if
                if kruxHeartbeat >= 10
                    kruxHeartbeat = 0
                    aetnKrux("heartbeat", "", "", video)
                end if

                if ( msg.GetIndex() = 10 or msg.GetIndex() MOD 30 = 0 or scrubbed = true) and msg.GetIndex() < video.totalVideoDuration/1000
                    addToContinueWatching(video, msg.GetIndex())
                end if

                if msg.GetIndex() >=  video.totalVideoDuration/1000 - 2
                    removeFromContinueWatching(video)
                end if

                current_cuepoint = getCurrentCuePoint(msg.GetIndex(), ads)
                current_cuepoint_str = itostr(current_cuepoint)

                if toStr(video.chapter) = "None" 'Shortform Clip
                    if video.chapterTracker[0].isStarted = false                   
                        aetnOmniture("", video, ["Video View", "Video Start"])
                        aetnKrux("event", "",  "Start", video)
                        video.chapterTracker[0].isStarted = true
                    end if

                    if video.chapterTracker[0].fireCompleteEvent = false and video.totalVideoDuration/1000 - msg.getIndex() < 3
                        video.videocalltype = "ContentComplete"
                        aetnOmniture("", video, ["Video Complete"])
                        aetnKrux("event", "", "End", video)
                        video.chapterTracker[0].fireCompleteEvent = true
                    end if
                else 'Full Episode
                    video.chapter = getCurrentChapter(msg.GetIndex(), video.chapterTracker)

                    'Play Ad if midroll ad exists and not played yet
                    if not ads[ current_cuepoint_str ] = Invalid
                        if not ads[ current_cuepoint_str ].played
                            player.Close()
                            'save the scrubedPastCuePoint variable globally.
                            m.scrubbedPastCuePoint = scrubbed
                            result = playSlot(current_cuepoint_str, ads, video)
                            if result = false
                                exit while
                            end if
                            return msg.GetIndex()
                        end if
                    else 
                        m.scrubbedPastCuePoint = scrubbed
                    end if


                    'fire approprate video events
                    if video.chapterTracker[video.chapter - 1].isStarted = false and msg.GetIndex() >= video.chapterTracker[video.chapter - 1].checkpoint 
                        video.chapterTracker[video.chapter - 1].isStarted = true
                        if video.chapter = 1
                            aetnOmniture("", video, ["Video View", "Video Start", "Episode Start"])
                            aetnKrux("event", "", "Start", video)
                        else 
                            if m.scrubbedPastCuePoint = false
                                aetnOmniture("", video, ["Video View", "Video Start"])
                                aetnKrux("event", "", "Start", video)
                            else
                                aetnOmniture("", video, ["Video View"])
                            end if
                        end if
                    end if


                    if video.chapterTracker[video.chapter - 1].fireCompleteEvent = false
                        if (video.chapter < video.chapterTracker.count())
                            if video.chapterTracker[video.chapter].checkpoint - msg.getIndex() < 3
                                video.videocalltype = "ContentComplete"
                                aetnOmniture("", video, ["Video Complete"])
                                aetnKrux("event", "", "End", video)
                                video.chapterTracker[video.chapter - 1].fireCompleteEvent = true
                            end if
                        else
                            duration = video.totalVideoDuration / 1000
                            remaining = duration - msg.getIndex()
                            if  remaining < duration * 0.02  and remaining > 3
                                if fireEpisodeCompleteEvent = false
                                    aetnOmniture("", video, ["Episode Complete"])
                                    aetnKrux("event", "", "End", video)
                                    fireEpisodeCompleteEvent = true
                                end if

                            else if remaining < 3
                                aetnOmniture("", video, ["Video Complete"])                            
                                video.chapterTracker[video.chapter - 1].fireCompleteEvent = true
                            end if
                        end if
                    end if

                end if

                previous_position = msg.GetIndex()

            else if msg.isScreenClosed()
            
                endPointStr = itostr(video.totalVideoDuration/1000)


                if not ads[endPointStr] = Invalid and video.totalVideoDuration/1000 < previous_position + 10
                    playSlot(endPointStr, ads, video)
                end if

                exit while

            end if
        end if
    end while

    player.Close()

    return -1

End Function


Function playSlot(current_cuepoint as String, ads as Object, video As Object)

    aetn_log("info", "appPlayer", "Playing Ad Slot " + current_cuepoint)

    aetn_remote_get(ads[ current_cuepoint ].slotImpressionUrl)

    for each creative in ads[ current_cuepoint ].creatives

        result = playCreative(creative, video)

        if not result
            return result
        end if
    end for

    if not ads[ current_cuepoint ] = Invalid 
        ads[ current_cuepoint ].played = true
    end if

    return true

End Function


Function playCreative(creative As Object, video As Object)
    
    if creative = invalid
        print "!!!!!!!!!!!!!!!BAD creative!!!!!!!!!!!!!!!!!!"
        return true
    end if

    result  = true
    canvas  = CreateObject("roImageCanvas")
    player  = CreateObject("roVideoPlayer")
    port    = CreateObject("roMessagePort")

    canvas.SetMessagePort(port)
    canvas.SetLayer(0, "#00000000")
    canvas.Show()

    'get vast ad url if creative is using wrapperUrl
    if not creative[ "wrapperUrl" ] = Invalid
        vast_xml = aetn_remote_get(creative[ "wrapperUrl" ])
                
        media = parseVastResponse(vast_xml)
        if not media = Invalid 
            creative [ "streams" ] = media[ "streams" ]
            creative [ "publisher_events" ] = media[ "publisher_events" ]
            creative [ "publisher_impressions" ] = media[ "publisher_impressions" ]

            if not media[ "duration" ] = 0
                creative [ "duration" ] = media[ "duration" ]
            end if
        end if
    end if
    

    if creative.streams <> invalid
        if creative.streams.count() = 1
            adVideo = {
                streamFormat: "mp4",
                stream: {url: creative.streams[0].url}
            }
        else 
            adVideo = {
                streamFormat: "mp4",
                streams: creative.streams,
                SwitchingStrategy : "full-adaptation"
            }
        end if
    else 
        if creative.type = "application/x-mpegURL"
            format = "hls"
        else
            format = "mp4"
        end if 

        adVideo = {
            streamFormat: format,
            stream: {url:  creative.url}
        }
    end if

    adVideo.StreamBitrates = [0]
    adVideo.StreamQualities = ["HD"]
    adVideo.SwitchingStrategy = "full-adaptation"


    player.SetMessagePort(port)
    player.SetCertificatesFile("common:/certs/ca-bundle.crt")
    player.InitClientCertificates()
    player.SetDestinationRect(canvas.GetCanvasRect())
    player.AddContent(adVideo)
    player.SetPositionNotificationPeriod(1)
    player.Play()

    aetn_log("trace", "appPlayer","play creative", creative, "ad creative object")
    aetn_log("trace", "appPlayer", "play Ad Video", adVideo, "ad video object")

    video.videocalltype = "Ad"
    video.videoadvertiser = creative._fw_advertiser_name
    video.videoadduration = creative.duration
    video.videoadtitle = creative._fw_creative_name
    adStarted = false
    

    quarter = creative.duration * .25
    half = creative.duration * .5
    third = creative.duration * .75

    cuepoints = {
        firstQuartile : false,
        midPoint : false,
        thirdQuartile : false
    }

    retry_play = true

    while true
    
        msg = wait(0, canvas.GetMessagePort())
        

        if type(msg) = "roVideoPlayerEvent"
            if msg.isFullResult()
                exit while
            
            else if msg.isRequestFailed()
                if retry_play
                    aetn_log("error", "appPlayer", "AD request failure, but will retry: " + msg.GetMessage())
                    player.Play()
                    retry_play = false
                else
                    aetn_log("error", "appPlayer", "AD request failure, final: " + msg.GetMessage())
                    exit while
                end if
            
            else if msg.isStatusMessage()
                if msg.GetMessage() = "start of play"
                    canvas.SetLayer(0, { color: "#00000000", CompositionMode: "Source" })
                    canvas.Show()
                end if
            
            else if msg.isPlaybackPosition()    
                'date = CreateObject("roDateTime")
                'print "PLAY POSITION: "; msg.GetIndex();"  time="; date.getMinutes();":";date.getSeconds()
                position = msg.GetIndex()

                if adStarted = false and position > 0
                    aetnOmniture("", video, ["Video Ad Start"])
                    adStarted = true

                    fireTrackingEvent(creative.events.defaultImpression)

                    if not creative["publisher_impressions"] = Invalid
                        for each imp in creative["publisher_impressions"]
                            fireTrackingEvent(imp)
                        end for
                    end if
                end if

                if quarter < position and cuepoints.firstQuartile = false                   
                    fireTrackingEvent(creative.events.firstQuartile)
                    cuepoints.firstQuartile = true

                    if not creative.publisher_events = Invalid
                        if not creative.publisher_events.firstQuartile = Invalid
                            for each pub_event in creative.publisher_events.firstQuartile
                                fireTrackingEvent(pub_event)
                            end for
                        end if
                    end if

                else if half < position and cuepoints.midPoint = false
                    fireTrackingEvent(creative.events.midPoint)
                    cuepoints.midPoint = true

                    if not creative.publisher_events = Invalid
                        if not creative.publisher_events.midPoint = Invalid
                            for each pub_event in creative.publisher_events.midPoint                            
                                fireTrackingEvent(pub_event)
                            end for
                        end if
                    end if

                else if third < position and cuepoints.thirdQuartile = false
                    fireTrackingEvent(creative.events.thirdQuartile)
                    cuepoints.thirdQuartile = true

                    if not creative.publisher_events = Invalid
                        if not creative.publisher_events.thirdQuartile = Invalid
                            for each pub_event in creative.publisher_events.thirdQuartile                                
                                fireTrackingEvent(pub_event)
                            end for
                        end if
                    end if
                end if
            end if          

        else if type(msg) = "roImageCanvasEvent"
            if msg.isRemoteKeyPressed()
                index = msg.GetIndex()
                if index = 0 or index = 2
                    result = false
                    exit while
                end if
            end if
        end if

    end while


    player.Stop()


    canvas.Close()

    if result and adStarted
        aetnOmniture("", video, ["Video Ad complete"])
        fireTrackingEvent(creative.events.complete)

        if not creative.publisher_events = Invalid
            if not creative.publisher_events.complete = Invalid
                for each pub_event in creative.publisher_events.complete
                    fireTrackingEvent(pub_event)
                end for
            end if
        end if

    end if

    return result
End Function


Function authorizeVideo(video As Object)

    port = CreateObject("roMessagePort")
    http = CreateObject("roUrlTransfer")
    http.SetPort(port)

    if(video.playURL_HLS = Invalid OR video.playURL_HLS = "")
        aetn_error("Error", "Video not available")
        return Invalid
    end if

    signature = aetn_remote_get( m.aetn_context.tpSignatureURL + "?url=" + http.urlEncode( video.playURL_HLS ) )
    stream_url = video.playURL_HLS + "&sig=" + signature.Trim()

    if strtobool(video.isBehindWall)
 
        authz = getAuthorization( video )

       if not isbool(authz)

            token = getShortMediaToken( video )
            
            if not isbool(token)
                ba = CreateObject("roByteArray")
                ba.FromBase64String(token.serializedToken)
                stream_url = stream_url + "&auth=" + http.urlEncode( ba.ToAsciiString() )
            else
                aetn_error("Error", "User not authorized.")
                return Invalid
            end if
        else

            token = getShortMediaToken( video )
            
            if not isbool(token)
                stream_url = stream_url + "&auth=" + token.serializedToken
            else
                aetn_error("Error", "User not authorized.")
                return Invalid
            end if
        end if

    end if

    return stream_url

End Function


Function parseCuePoints(video As Object)

    aetn_log("trace", "appPlayer", "cuepoints", video.cuepoints)

    if isnullorempty(video.cuepoints)
        return []
    else
        '00:11:02,00:20:22,00:29:20,00:37:42
        cuepoints = strSplit(video.cuepoints, ",")
        cuepointArray = CreateObject("roArray", cuepoints.count(), true)
        chapter = 0
        for each cuepoint in cuepoints
            t = strSplit(cuepoint, ":")
            'calculate seconds form hh:mm:ss" fromat
            h = t[0].toInt()
            m = t[1].toInt()
            s = t[2].toInt()
            seconds = h * 3600 + m * 60 + s
            point = {}
            point.seconds = seconds
            cuepointArray.push(point)
        end for
        return cuepointArray
    end if
End Function


Function getCurrentCuePoint(position As Integer, ads As Object)

    current_cuepoint = 0

    for each ad in ads
        time = Int(Val(ad))

        if time <= position
            if time > current_cuepoint
                current_cuepoint = time
            end if
        end if
    end for

    return current_cuepoint
End Function

Function getCurrentChapter(position As Integer, chapterChecker)
    For i = 0  To chapterChecker.count() - 1 Step 1
        if position >= chapterChecker[i].checkpoint 
            if i < chapterChecker.count() - 1 
                if position < chapterChecker[i+1].checkpoint
                    return i + 1
                end if
            else 
                return i + 1
            end if
        end if
    End For
    return 1
End Function

Function getCloseCaptionUrl(streamUrl As String)
    smilStr = aetn_remote_get(streamUrl + "&format=smil")
    xml = CreateObject("roXMLElement")
    xml.parse(smilStr)

    for each item in xml.body.seq.par
            captionUrl = item.textstream@src
            if isnonemptystr(captionUrl)
                return captionUrl
        end if
    end for

    return ""

End Function


Function fireTrackingEvent(eventUrl as String)
    result = false
    xfer = CreateObject("roURLTransfer")
    xfer.SetPort(m.port) ' need a message port for redirect handling
    xfer.SetURL(eventUrl)
    aetn_log("trace", "aetnPlayer", "fireTrakcingEvent: with tracking url " + eventUrl)
      
    result = xfer.AsyncGetToString()
    if result
        ' if our collection of xfers is getting too big, dump the oldest ones
        if m.xfers = invalid
            m.xfers = CreateObject("roArray", 0, true)
        end if

        while m.xfers.Count() > 40
          m.xfers.Shift()
        end while
        ' maintain a reference to the xfer so that the request can complete
        ' if a roURLTransfer object is destroyed before the request completes, it will fail
        m.xfers.Push(xfer)
        aetn_log("trace", "aetnPlayer", "fireTrakcingEvent: async request sent with ID " + xfer.GetIdentity().ToStr())
        aetn_log("trace", "aetnPlayer", "fireTrackingEvent: xfer count " + m.xfers.Count().ToStr())
    else
        aetn_log("warn", "aetnPlayer", "fireTrakcingEvent: async request failed")
    end if
      
    return result
End Function
