Function addToContinueWatching(videoObj as Dynamic, video_position as Integer) as Boolean
    
    aetn_log("info", "aetnUtil", "Registry Object")

    continueWatchingStr = get_registry_section(m.aetn_context.rokulocalstorage.section, m.aetn_context.rokulocalstorage.continuekey)

    if continueWatchingStr = Invalid
        continueWatchingList = []
    else
        continueWatchingList = ParseJSON(continueWatchingStr)

        index = 0
        matchingIndex = Invalid
        for each item in continueWatchingList
            if item.AETNContentID = videoObj.AETNContentID
                aetn_log("debug", "aetnUtilsContinueWatching:addToContinueWatching", "Item printed after match: ", item, "Item to be deleted")
                continueWatchingList.Delete(index)
                index = index - 1
            end if 
            index = index + 1
        next

    end if

    if type(continueWatchingList) <> "roArray"
        aetn_error("There was an error retrieving your unfinished videos")
        continueWatchingList = []
    end if

    if continueWatchingList.Count() >= 20
        continueWatchingList.Pop()
    end if

    'serialize watchlist object
    continueWatchingObj = {}

    continueWatchingObj.Title = videoObj.title
    continueWatchingObj.HDPosterUrl = videoObj.HDPosterUrl
    continueWatchingObj.SDPosterUrl = videoObj.SDPosterUrl
    continueWatchingObj.ContentType = videoObj.ContentType
    continueWatchingObj.AETNContentType = "video"
    continueWatchingObj.AETNContentID = videoObj.AETNContentID
    continueWatchingObj.description = videoObj.description

    if video_position <> Invalid
        continueWatchingObj["lastPlaybackPosition"] = video_position
    else
        continueWatchingObj["lastPlaybackPosition"] = 0
    end if

    continueWatchingList.Unshift(continueWatchingObj)

    set_registry_section(m.aetn_context.rokulocalstorage.section, m.aetn_context.rokulocalstorage.continuekey, continueWatchingList)

    set_registry_flush(m.aetn_context.rokulocalstorage.section)

    m.refreshHomeScreen = true
    
    return true
    
End Function


Function removeFromContinueWatching(videoObj as Dynamic) as Boolean
    
    continueWatchingStr = get_registry_section(m.aetn_context.rokulocalstorage.section, m.aetn_context.rokulocalstorage.continuekey)

    if continueWatchingStr = Invalid
        return false
    else
        continueWatchingList = ParseJSON(continueWatchingStr)
        index = 0
        for each item in continueWatchingList
            if item.AETNContentID = videoObj.AETNContentID
                aetn_log("debug", "aetnUtilsContinueWatching:removeFromContinueWatching", "item printed after match", item, "Item to be deleted")
                continueWatchingList.Delete(index)
                index = index - 1
            end if 
            index = index + 1
        next       
    end if

    set_registry_section(m.aetn_context.rokulocalstorage.section, m.aetn_context.rokulocalstorage.continuekey, continueWatchingList)
    set_registry_flush(m.aetn_context.rokulocalstorage.section)
    
    m.refreshHomeScreen = true

    return true

End Function


Function isPresentInContinueWatching(videoObj as Dynamic) as Dynamic
    continueWatchingStr = get_registry_section(m.aetn_context.rokulocalstorage.section, m.aetn_context.rokulocalstorage.continuekey)

    if continueWatchingStr <> Invalid
        continueWatchingList = ParseJSON(continueWatchingStr)
        for each item in continueWatchingList
            if item.AETNContentID = videoObj.AETNContentID
                return true
            end if 
        next
    end if
    
    return false

End Function

Function lastPlaybackPositionInContinueWatching(videoObj as Dynamic) as Dynamic
    
    continueWatchingStr = get_registry_section(m.aetn_context.rokulocalstorage.section, m.aetn_context.rokulocalstorage.continuekey)

    if continueWatchingStr <> Invalid
        continueWatchingList = ParseJSON(continueWatchingStr)
        if continueWatchingList <> Invalid
            for each item in continueWatchingList
                if item.AETNContentID = videoObj.AETNContentID
                    return item.lastPlaybackPosition
                end if 
            next
        end if
    end if
    
    return Invalid

End Function

