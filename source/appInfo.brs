Function showInfoDialog() As Void 

    port = CreateObject("roMessagePort")
    dialog = CreateObject("roMessageDialog")
    dialog.SetMessagePort(port) 
    dialog.SetTitle("Info")
 
    dialog.AddButton(0, "About " + m.aetn_context.rokuinfo.brand)
    dialog.AddButton(1, "Feedback and Support")
    dialog.AddButton(2, "Legal")
    dialog.AddButton(3, "Done")

    dialog.EnableBackButton(true)
    dialog.Show()
    
    while True
        dlgMsg = wait(0, dialog.GetMessagePort())
        if type(dlgMsg) = "roMessageDialogEvent"
            if dlgMsg.isButtonPressed()
                if dlgMsg.GetIndex() = 0
                	showAboutHistoryDialog(dialog)
                else if dlgMsg.GetIndex() = 1
                	showFeedbackAndSupportDialog(dialog)
                else if dlgMsg.GetIndex() = 2
                	showLegalDialog(dialog)
                else
                    exit while
                end if
            else if dlgMsg.isScreenClosed()
                exit while
            end if
        end if
    end while

End Function

Function showAboutHistoryDialog(parent as object) As Void
    
    port = CreateObject("roMessagePort")
    dialog = CreateObject("roMessageDialog")
    dialog.SetMessagePort(port) 
    dialog.SetTitle("About " + m.aetn_context.rokuinfo.brand )
    dialog.SetText(m.aetn_context.rokuinfo.about)
 
    dialog.AddButton(0, "Done")
    dialog.EnableBackButton(true)
    dialog.Show()
    
    while True
        dlgMsg = wait(0, dialog.GetMessagePort())
        if type(dlgMsg) = "roMessageDialogEvent"
            if dlgMsg.isButtonPressed()
                if dlgMsg.GetIndex() = 0
                    parent.Close()
                    exit while
                end if
            else if dlgMsg.isScreenClosed()
                exit while
            end if
        end if
    end while

End Function

Function showFeedbackAndSupportDialog(parent as object) As Void 
    
    port = CreateObject("roMessagePort")
    dialog = CreateObject("roMessageDialog")
    dialog.SetMessagePort(port) 
    dialog.SetTitle("Feedback and Support")
    dialog.SetText("If you are experiencing trouble with the app, please contact us at watchapps@aenetworks.com")
 
    dialog.AddButton(0, "Done")

    dialog.EnableBackButton(true)
    dialog.Show()    

    while True
        dlgMsg = wait(0, dialog.GetMessagePort())
        if type(dlgMsg) = "roMessageDialogEvent"
            if dlgMsg.isButtonPressed()
                if dlgMsg.GetIndex() = 0
                    parent.Close()
                    exit while
                end if
            else if dlgMsg.isScreenClosed()
                exit while
            end if
        end if
    end while

End Function

Function showLegalDialog(parent as object) As Void 
    
    port = CreateObject("roMessagePort")
    dialog = CreateObject("roMessageDialog")
    dialog.SetMessagePort(port) 
    dialog.SetTitle("Legal Notices")
    dialog.SetText("A+E Terms of Use: " + m.aetn_context.termsURL) 
    dialog.SetText("A+E Privacy Policy: " + m.aetn_context.privacyURL)
 
    dialog.AddButton(0, "Done")

    dialog.EnableBackButton(true)
    dialog.Show()   

    While True
        dlgMsg = wait(0, dialog.GetMessagePort())
        if type(dlgMsg) = "roMessageDialogEvent"
            if dlgMsg.isButtonPressed()
                if dlgMsg.GetIndex() = 0
                    parent.Close()
                    exit while
                end if
            else if dlgMsg.isScreenClosed()
                exit while
            end if
        end if
    end while

End Function
