Function getAds(video As Object)
    url = strReplace(m.aetn_context.fwRokuAPI, ";;", ";")

    if m.aetn_token <> Invalid and m.mvpdFWMap[m.aetn_token.mvpd] <> Invalid
        url = url + "_fw_ae=" + m.mvpdFWMap[m.aetn_token.mvpd] + ";"
    else 
        url = url + ";"
    end if

    url = url + "ptgt=a&slau=preroll&tpcl=preroll&slid=preroll1&tpos=0;"

    index = 0
    for each cuepoint in video.cuepoints
        url = url + "ptgt=a&slau=midroll&tpcl=midroll&slid=midroll" + itostr(index) + "&cpsq&tpos=" + itostr(cuepoint.seconds) + ";"
        index = index + 1
    end for

    length = Int(Int(video.totalVideoDuration) / 1000)

    if video.programId = Invalid
        video.programId = ""
    end if

    url = strReplace(url, "|id|",video.programId) + "ptgt=a&slau=postroll&tpcl=postroll&slid=postroll1&tpos=" + itostr(length) + ";"
    url = strReplace(url, "vdur=&", "vdur=" + itostr(length) + "&")

    

    smart_xml = aetn_remote_get(url)
    
    if smart_xml = ""
        return {}
    end if

    ads = parseSmartXML(smart_xml)

    if ads = Invalid
        return {}
    end if
    'printAnyAA(3, ads)
    return ads

End Function


Function parseSmartXML(smart_xml as String)
    
    xml = CreateObject("roXMLElement")

    if not xml.Parse(smart_xml)
        aetn_log("error", "aetnFreeWheel", "Can't parse SmartXML")
        return Invalid
    end if

    if xml.ads = Invalid
        aetn_log("error", "aetnFreeWheel", "No Ads Found")
        return Invalid
    end if

    if xml.siteSection = Invalid
        aetn_log("error", "aetnFreeWheel", "No site section Found")
        return Invalid
    end if

    if xml.siteSection.videoPlayer = Invalid
        aetn_log("error", "aetnFreeWheel", "No site section video player Found")
        return Invalid
    end if

    if xml.siteSection.videoPlayer.videoAsset = Invalid
        aetn_log("error", "aetnFreeWheel", "No Video Asset Found")
        return Invalid
    end if

    if xml.siteSection.videoPlayer.videoAsset.adSlots = Invalid
        aetn_log("error", "aetnFreeWheel", "No Ad Slot Found")
        return Invalid
    end if

    creatives = parseCreatives(xml.ads.GetChildElements())

    adsWrapper = {}

    adsWrapper.slots = parseAdSlots(xml.siteSection.videoPlayer.videoAsset.adSlots.GetChildElements(), creatives)

    adsWrapper.callbackUrl = xml.siteSection.videoPlayer.videoAsset.eventCallbacks.eventCallback@url

    return adsWrapper

End Function


Function parseCreatives(ads as Object)

    creatives = {}

    for each ad in ads

        if not ad.creatives = Invalid and not ad.creatives.creative = Invalid
            'if there is HLS renditon, use HLS rendtion, otherwise use the 1st rendition
            index = 0
            hasHls = false
            streams = []

            i = 0
            for each creativeRendition in ad.creatives[0].creative[0].creativeRenditions[0].creativeRendition
                if creativeRendition.asset@mimeType = "application/x-mpegURL"
                    hasHls = true
                    index = i
                else 
                    if ad.creatives.creative[0].creativeRenditions[0].creativeRendition[index].asset <> Invalid
                        if ad.creatives.creative[0].creativeRenditions[0].creativeRendition[i].asset@url <> Invalid
                            stream= {}
                            stream.bitrate = ad.creatives.creative[0].creativeRenditions[0].creativeRendition[i].asset@bytes
                            stream.url = ad.creatives.creative[0].creativeRenditions[0].creativeRendition[i].asset@url
                            stream.quality = true
                            streams.push(stream)
                        end if
                    end if
                end if
                i = i + 1
            end for

            creative = {
                id: ad.creatives[0].creative[0]@creativeId,
                type: ad.creatives.creative[0].creativeRenditions[0].creativeRendition[index].asset@mimeType,
                duration: Int(Val(ad.creatives.creative[0]@duration))
            }

            if hasHls = true
                creative[ "url" ] = ad.creatives.creative[0].creativeRenditions[0].creativeRendition[index].asset@url
            else if streams.count() > 0
                creative["streams"] = streams
            else 
                if ad.creatives[0].creative[0].creativeRenditions[0].creativeRendition[index]@wrapperUrl <> invalid
                    creative[ "wrapperUrl" ] = ad.creatives[0].creative[0].creativeRenditions[0].creativeRendition[index]@wrapperUrl
                end if 
            end if

       ''     if not ad.creatives[0].creative[0].creativeRenditions[0].creativeRendition[index]@wrapperUrl = Invalid
        ''        creative[ "wrapperUrl" ] = ad.creatives[0].creative[0].creativeRenditions[0].creativeRendition[index]@wrapperUrl
         ''   else
          ''      creative[ "url" ] = ad.creatives.creative[0].creativeRenditions[0].creativeRendition[index].asset@url
           '' end if


            for each parameter in ad.creatives[0].creative[0].parameters.GetChildElements()
                creative[ parameter@name ] = parameter.GetText()
            end for

            creatives[ creative.id ] = creative

        end if

    end for

 
    return creatives
End Function

Function parseVastResponse(vast_xml)

    xml = CreateObject("roXMLElement")

    if not xml.Parse(vast_xml)
        aetn_log("error", "aetnFreeWheel", "Can't parse Vast XML")
        return Invalid
    end if

    if xml.ad = Invalid
        aetn_log("error", "aetnFreeWheel", "No Ad Found")
        return Invalid
    end if


    if xml.ad.wrapper.vastadtaguri.gettext() = ""
        aetn_log("trace", "aetnFreeWheel","parseVastResponse", vast_xml, "Standard Vast XML")
        return parseVastXML(xml)
    end if

    aetn_log("trace", "aetnFreeWheel","parseVastResponse", vast_xml, "Wrapper Vast XML")

    media = parseVastWrapper(xml)

    if media = Invalid
        return Invalid
    end if


    if not xml.ad.wrapper.creatives.creative.linear.TrackingEvents = Invalid
        for each ev in xml.ad.wrapper.creatives.creative.linear.TrackingEvents.Tracking

            if media["publisher_events"][ ev@event ] = Invalid
                media["publisher_events"][ ev@event ] = []
            end if

            media["publisher_events"][ ev@event ].push( ev.gettext() )
        end for
    end if


    if not xml.ad.wrapper.Impression = Invalid
        for each impression in xml.ad.wrapper.Impression
            media["publisher_impressions"].push( impression.gettext() )
        end for
    end if

    return media
End Function


Function parseVastWrapper(xml)

    if xml.ad.wrapper.vastadtaguri = Invalid
        aetn_log("error", "aetnFreeWheel", "No Wrapper Found")
        return Invalid
    end if

    vast_xml = aetn_remote_get(xml.ad.wrapper.vastadtaguri.gettext())

    media = parseVastResponse(vast_xml)

    return media

End Function


Function parseVastXML(xml)

    if xml.ad.inline = Invalid
        aetn_log("error", "aetnFreeWheel", "No Inline Section Found")
        return Invalid
    end if

    if xml.ad.inline.creatives = Invalid
        aetn_log("error", "aetnFreeWheel", "No Inline Creatives Section Found")
        return Invalid
    end if

    if xml.ad.inline.creatives.creative = Invalid
        aetn_log("error", "aetnFreeWheel", "No InLIne Creatives Creative Section Found")
        return Invalid
    end if

    if xml.ad.inline.creatives.creative.linear = Invalid
        aetn_log("error", "aetnFreeWheel", "No inline creatives creative linear section found")
        return Invalid
    end if

    if xml.ad.inline.creatives.creative.linear.mediaFiles = Invalid
        aetn_log("error", "aetnFreeWheel", "No inline creatives creative linear mediaFiles section found")
        return Invalid
    end if

    if xml.ad.inline.creatives.creative.linear.mediaFiles.mediaFile = Invalid
        aetn_log("error", "aetnFreeWheel", "No inline creatives creative linear mediaFiles mediaFile section found")
        return Invalid
    end if

    streams = []
    for each mediaFile in xml.ad.inline.creatives.creative.linear.mediaFiles.mediaFile
        if mediaFile@type = "video/mp4"
            stream= {}
            stream.bitrate = mediaFile@bitrate
            stream.url = mediaFile.gettext()
            stream.quality = true
            streams.push(stream)
        end if
    end for


    events = {}
    if not xml.ad.inline.creatives.creative.linear.TrackingEvents = Invalid
        for each ev in xml.ad.inline.creatives.creative.linear.TrackingEvents.Tracking

            if events[ ev@event ] = Invalid
                events[ ev@event ] = []
            end if

            events[ ev@event ].push( ev.gettext() )
        end for
    end if
    

    impressions = []
    if not xml.ad.inline.Impression = Invalid
        for each impression in xml.ad.inline.Impression
            impressions.push( impression.gettext() )
        end for
    end if

    duration = xml.ad.inline.creatives.creative.linear.duration.gettext()
    hours = mid( duration, 1, 2 )
    minutes = mid( duration, 4, 2 )
    seconds = mid( duration, 7, 2 )

    media = {
        streams : streams,
        publisher_events : events,
        publisher_impressions : impressions,
        duration : ( Val(hours) * 60 * 60 ) + ( Val(minutes) * 60 ) +  Val(seconds)
    }

    return media

End Function


Function parseAdSlots(slots as Object, creatives as Object)

    adSlots = {}

    for each slot in slots
    
        s = {
            type: slot@timePositionClass,
            time: Int(Val(slot@timePosition)),
            slotImpressionUrl: slot.eventCallbacks.eventCallback@url
            played: false
        }

        s.creatives = parseAdReferences( slot.selectedAds.GetChildElements(), creatives)

        adSlots[ slot@timePosition ] = s

    end for

    return adSlots
End Function


Function parseAdReferences(refs as Object, creatives as Object)

    ads = []

    for each adref in refs

        creative = creatives[ adref@creativeId ]

        if not creative = Invalid
            creative.events = parseAdEvents(adref.eventCallbacks.GetChildElements())
            
        end if

        ads.push(creative)
    end for

    return ads

End Function


Function parseAdEvents(events as Object)

    ev = {}

    for each event in events

        if event@type = "IMPRESSION"

            if event@name = "defaultImpression"
                ev[ event@name ] = event@url
            else if event@name = "firstQuartile"
                ev[ event@name ] = event@url
            else if event@name = "midPoint"
                ev[ event@name ] = event@url
            else if event@name = "thirdQuartile"
                ev[ event@name ] = event@url
            else if event@name = "complete"
                ev[ event@name ] = event@url
            end if

        end if

    end for

    return ev
End Function

