Function preSearchResultsScreen()

    port = CreateObject("roMessagePort")

    screen = CreateObject("roPosterScreen")
    screen.SetMessagePort(port)
    screen.SetTitle("History")
    screen.SetDisplayMode("scale-to-fill")
    screen.SetListStyle("flat-category")

    return screen

End Function


Function showSearchResultsScreen(screen As Object, contentObject As Object) As Object

    if validateParam(screen, "roPosterScreen", "showShowsScreen") = false return -1

    screen.SetBreadcrumbText("Search", contentObject.searchTerm)

    result_size = "50"

    searchTerm = strReplace(contentObject.searchTerm, " ", "%20")
    searchTerm = strReplace(searchTerm, "'", "")

    url = m.aetn_context.acsEndPointURL + "/search?return-fields=title,rating,total_video_duration,platform_id,description,video_url_hls,video_url_pd,is_close_caption,thumbnail_image_url&size=50&bq=(or%20description:'" + url_encode(url_decode(searchTerm)) + "'%20title:'" + url_encode(url_decode(searchTerm)) + "')"
    aetn_log("trace", "appSearchResult", "search url", url, "search url")
    feedJSON = aetn_remote_get(url)
    
    results = ParseJSON(feedJSON)

    list = CreateObject("roArray", 10, true) 

    screenDetails = {}
    screenDetails.searchTerm = searchTerm
    
    if results <> Invalid and results.hits <> Invalid and results.hits.hit <> Invalid
        if results.hits.hit.count() = 0
            aetnOmniture("SearchResults", screenDetails, ["Internal Search"])
            aetn_error("Search", "No Items found", "Go Back to Search")
            return 1
        else

            For Each item in results.hits.hit 
                o = CreateObject("roAssociativeArray")
                if item.data.thumbnail_image_url.count() = 0
                    show = findShowBySearchResultTitle(item.data.title[0], contentObject.shows)
                    if(show <> Invalid)
                        o.show = show
                        o.HDPosterUrl = show.HDPosterUrl
                        o.SDPosterUrl = show.SDPosterUrl
                        o.ContentType = "show"
                        o.Title = show.Title
                    end if
                else            
                    o.ContentType = "video"    
                    o.AETNContentType = "video" 
                    o.AETNContentId = item.data.platform_id[0]      
                    o.HDPosterUrl = item.data.thumbnail_image_url[0]
                    o.SDPosterUrl = item.data.thumbnail_image_url[0]

                    if item.data.is_close_caption.count() > 0
                        o.isCloseCaption = item.data.is_close_caption[0]
                    end if
                
                    if item.data.total_video_duration.count() > 0
                      o.totalVideoDuration = val(item.data.total_video_duration[0])
                    end if

                    if item.data.title.count() > 0
                        o.Title = item.data.title[0]
                    end if
                
                    o.playURL_HLS = item.data.video_url_hls[0]
                end if

                o.Description = item.data.description[0]
                o.ShortDescriptionLine1 = item.data.title[0]
                o.ShortDescriptionLine2 = item.data.description[0]
                 
                list.Push(o)
            End For
            
        end if
    end if

    screen.SetContentList(list)
    screen.Show() 
    
    aetnOmniture("SearchResults", screenDetails, ["Internal Search"])
    aetnKrux("screen", "Search", "", screenDetails)
    
    while true

        msg = wait(0, screen.getMessagePort())

        if msg.isScreenClosed()
        
            return -1
         
        else if msg.isListItemSelected()
        
            aetn_log("info", "appSearchResult", msg.GetMessage() + " idx: " + itostr(msg.GetIndex()))
        
            index = msg.GetIndex()

            if list[index].contentType = "show"
                aetn_show_screen(list[index].show)
            else if list[index].contentType = "video"
                aetn_show_screen(list[index])
            end if
        
        end if

    end while
End Function


Function findShowBySearchResultTitle(title As String, shows As Object)
    title = LCase(title)

    for each show in shows 
        showname = LCase(show.AETNContentID)
        if showname = title
            return show
        end if    
    end for

    return Invalid
End Function